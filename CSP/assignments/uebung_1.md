# Übung 1
## Einführung in die Cybersicherheitspolitik

> This has the wiff of August 1945. Someone just used a new weapon. And this weapon will not be put back into the box.
>
> $\qquad\qquad\qquad\qquad\qquad\qquad\qquad\qquad\qquad\qquad$ - Michael Hayden 

Der Vergleich von der post-Stuxnet Realität mit dem August des Jahres 1945 zeigt sowohl Ähnlichkeiten beider Ereignisse als auch Unterschiede auf.  
Wie damals, kurz vor dem Ende des zweiten Weltkrieges, wurde auch bei der Urananreicherungsanlage in Natanz, Iran erstmalig eine neuartige Waffe mit unermesslichen Folgen eingesetzt.  
Es wird die Atombombe mit einem Computer-Virus verglichen, doch ist dies berechtigt? Wie kann ein Programm welches vermeintlich nur im cyberspace existiert vergleichbaren Schaden anrichten wie die schlimmste Waffe die der Mensch je erzeugt hat?

Als die amerikanische und israelische Regierung mitbekommen haben, dass der Iran es sich zum Ziel gemacht hat, genau jene schlimmste Waffe mit ihrem Atomprogramm zu ihrem Nutzen zu machen, fühlten sie sich dazu verpflichtet einzugreifen. Damit standen sie vor einer enorm schwierigen Entscheidung, denn viele Optionen hatten sie nicht. Entweder sie bombardierten die Urananreicherungsanlage in Natanz worauf die Situation mit hoher Wahrscheinlichkeit zu einem internationalen Krieg zu eskalieren drohte oder sie mussten sich etwas anderes einfallen lassen.

Um einen Krieg mit allen Mitteln zu verhindern, haben sie sich für eine neuartige unauffällige Angriffstaktik entschieden, nämlich für einen Computer-Virus der die Zentrifugen in Natanz unbemerkt befallen und schlussendlich zerstören sollte. 
Im Kontrast zum Bombardement von Hiroshima und Nagasaki wurden durch diese neuartige Kriegsführung unzählige zivile Leben verschont, was einen in Versuchung bringt zu behaupten, dass diese neue Dimension des Krieges eine friedlichere sei als jene zuvor, da sie weniger Verwüstung anrichtet.

Doch dies zu glauben ist töricht, wie vermeintlich der Cyberspace keine zivile Opfer mit sich bringt wie zum Beispiel durch die Zerstörungskraft einer Atombombe, hängt der Kollateralschaden jener neuartigen Cyberwaffen ganz allein von der Absicht des Angreifers ab. Setzt dieser sich zum Ziel mehr Schaden als eine Atombombe anzurichten, so wird es ihm mit genügend finanziellen Mittelln und Geduld möglich sein dies auch zu erreichen. Denn die heutige, geschweige von der morgigen, Infrastruktur ist zunehmend von computergesteuerten Elementen abhängig, welche eine kritische Rolle im Zusammenspiel des Systems übernehmen. So hätte zum Beispiel ein Angriff auf ein staatliches Stromnetz, welcher in einem nationalen Blackout von mehreren Tagen resultiert, verheerende Folgen auf das Gesundheitswesen, das Kommunikationsnetz und die Notfalldienste, da wir für fast alles Strom benötigen.

Es lässt sich also sagen, dass die neuen Praktiken des Cyberwarfares potentiell noch gefährlicher sein können als atomare Waffen selbst, da wir in unserer Gesellschaft immer vernetzter sind und zukünftig werden, was uns dementsprechend verwundbar macht.