#include <stdio.h>

int main(int argc, char *argv[]) {
    /*
    int x = 0x00000000;
    printf("0x0100 = %d\n", 0x0100);
    printf("0x00000000 = %d\n", x);
    printf("!222 = %d\n", !222);
    printf("!0x0100 = %d\n", !0x0100);
    printf("!0x0000 = %d\n", !0x0000);

    int x = 0xE;
    
    int byte0 = (x & 0x55);
    int byte1 = (x & 0x55 << 8);
    int byte2 = (x & 0x55 << 16);
    int byte3 = (x & 0x55 << 24);

    int ans = !(!(byte0 | byte1 | byte2 | byte3));

    printf("x = 0x%x\n", x);
    printf("byte0 = %x\n", byte0);
    printf("byte1 = %x\n", byte1);
    printf("byte2 = %x\n", byte2);
    printf("byte3 = %x\n", byte3);
    printf("ans = 0x%x = %d\n", ans, ans);

    int byte = (x & ( 0x55 | 0x55 << 8 | 0x55 << 16 | 0x55 << 24));
    printf("ans2 = 0x%x\n", !!byte);

    int x = 0x80000000;
    int n = 3;
    int shift = n << 3;
    int mask = 0xff;

    int shx = x >> shift;
    int maskx = shx & mask;

    printf("0x%x\n", maskx);
    */

    /*
    int x = 0;
    // printf("%d\n", ~0); = -1
    // printf("%d\n", ~0^1); = -2
    // printf("%d\n", 0^1); = 1
    int trivial = 1^x | 0^x;
    printf("%d\n", ~trivial^0^x);
    printf("%d\n", trivial);


    printf("0x%x\n", ~(0x80<<24));

    int x = -8;
    int bit32 = 1 << 31;

    int isNeg = x & bit32;

    int anti_x = ~x + 1;

    int ans = 1^x ^ isNeg;
    printf("%x\n", ans);

        ans = 0^ anti_x ^ isNeg;
        ans = ans << 1 >> 1;
    printf("%x\n", ans);
    // printf("%d\n", 1 ^ anti_x ^ isNeg);

    int a = 0x91234567;
    int b = 0x89abcedf;

    int smaller = a & b;
    int greater = a | b;
    int difference = a ^ b;

    printf("a = %d\nb = %d\n", a, b);
    printf("smaller = %x\ngreater = %x\ndifference = %d\n", smaller, greater, difference);


    //int x = 1073741824;
    int x = -22;
    printf("%x\n", x * 5/8);
    printf("%x\n", () >> 3);
    //printf("%d\n", 


    int x = 0x8f000000;
    printf("%x\n", x);
    int y = x | x << 1;
    printf("%x\n", y);


    int x = 0xf0000000;
    int isNeg = x & (1 << 31);
    int correction = !!isNeg;

    int x5 = (x << 2) + x;
    int res = (x5 >> 3) + correction;

    int gold = x * 5/8;

    printf("0x%x\n0x%x\n0x%x\n0x%x\n0x%x\n", x, isNeg, correction, res, gold);

    int x = 0x8fffffff;
    //x = 0x0234f;
    
    int anti = ~x + 1;
    int isNeg = !!(x & (1 << 31));

    printf("0x%x\n", isNeg);
    isNeg = 1 ^ anti ^ isNeg;
    printf("0x%x\n", isNeg);

    isNeg = 0 ^ x ^ isNeg;
    printf("0x%x\n", isNeg);

    int out = isNeg;

    printf("%d\n%d\n%d\n\n", x, anti, out);
    printf("0x%x\n0x%x\n0x%x\n", x, anti, out);

    */


    int x = 0x80000000;  // Tmin
    int y = 0x0;         // 0x0

    int anti_y = ~y + 1; // 0x0

    int sub = x + anti_y;

    // if (test) out = a; else out = b;
    int isNeg = sub & (1 << 31);
    int test = isNeg;
    int b = !!isNeg;
    int a = !isNeg;
    int output = (((test << 31) >> 31) & a) | (((!test << 31) >> 31) & b);

    printf("out = %x\n", output);
}
