#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * HELPER FUNCTIONS
 */

void print_help() {
    printf("USAGE:\n\n");
    printf("  cv OPTION NUMBER\n");
    printf("\n");
    printf("OPTION:\n\n");
    printf("  -db\tdecimal to binary\n");
    printf("  -dh\tdecimal to hexadecimal\n");
    printf("\n");
    printf("  -bd\tbinary to decimal\n");
    printf("  -bh\tbinary to hexadecimal\n");
    printf("\n");
    printf("  -hd\thexadecimal to decimal\n");
    printf("  -hb\thexadecimal to binary\n");
}

void print_long_to_binary(long input) {
    int firstOneSeen = 0;
    int out = 0;
    int ith_bit = 0;

    int bits = 63; // 31;
    for (int i = bits; i >= 0; i--) {
        ith_bit = (input >> i) & 1;

        if (!firstOneSeen)
            firstOneSeen = ith_bit;

        if (!firstOneSeen)
            continue;

        printf("%d", ith_bit);
    }

    if (!firstOneSeen)
        printf("%d", 0);
}

void decimal_to_hex(char *arg) {
    printf("%s = 0x%x\n", arg, strtol(arg, NULL, 10));
}

void hex_to_decimal(char *arg) {
    printf("%s = %d\n", arg, strtol(arg, NULL, 0));
}

void binary_to_hex(char *arg) {
    printf("%s = 0x%x\n", arg, strtol(arg, NULL, 2));
}

void hex_to_binary(char *arg) {
    printf("%s = ", arg);

    print_long_to_binary(strtol(arg, NULL, 16));

    printf("\n");
}

void binary_to_decimal(char *arg) {
    printf("%s = %d\n", arg, strtol(arg, NULL, 2));
}

void decimal_to_binary(char *arg) {
    printf("%s = ", arg);
    print_long_to_binary(strtol(arg, NULL, 10));
    printf("\n");
}

/*
 * APPLICATION
 */

int main(int argc, char *argv[]) {
    if (argc == 1 || argc == 2 || argv[1][0] != '-' || strlen(argv[1]) != 3) {
        print_help();
        exit(1);
    }
    
    switch (argv[1][1]) {
        case 'b':
            switch (argv[1][2]) {
                case 'h':
                    binary_to_hex(argv[2]);
                    break;
                case 'd':
                    binary_to_decimal(argv[2]);
                    break;
                default:
                    print_help();
                    exit(1);
                    break;
            }
            break;

        case 'h':
            switch (argv[1][2]) {
                case 'b':
                    hex_to_binary(argv[2]);
                    break;
                case 'd':
                    hex_to_decimal(argv[2]);
                    break;
                default:
                    print_help();
                    exit(1);
                    break;
            }
            break;
        
        case 'd':
            switch (argv[1][2]) {
                case 'b':
                    decimal_to_binary(argv[2]);
                    break;
                case 'h':
                    decimal_to_hex(argv[2]);
                    break;
                default:
                    print_help();
                    exit(1);
                    break;
            }
            break;

        default:
            print_help();
            exit(1);
            break;
    }
    
    return 0;
}
