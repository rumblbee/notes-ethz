# 3 Direct Methods for Linear Least Square Problems

### 3.0.1 Overdetermined LSE

overdetermined $Ax = b \implies A \in \R^{m,n} \quad m \geq n$ 

#### 3.0.1.1 Linear Parameter Estimation in 1D

**Law:** $y = \alpha x + \beta$ for some unknown parameters $\alpha, \beta \in \R$

**Measured:** $(x_i, y_i) \in \R^2 \quad i = 1 \dots m, \ m \gt 2 \\ y_i = \alpha x_i + \beta \ \forall i \implies \text{LSE}$

**Theoretically:** (exact measurements)
$$
\begin{bmatrix}
x_1 & 1 \\
x_2 & 1 \\
\vdots & \vdots \\
&\\
\vdots & \vdots \\
x_n & 1 
\end{bmatrix}

\begin{bmatrix}
\alpha \\
\beta
\end{bmatrix}
=
\begin{bmatrix}
y_1 \\
y_2 \\
\vdots \\
\\
\vdots \\
y_n
\end{bmatrix}

\iff Ax=b \quad A \in \R^{m,2}, b \in \R^m, x \in \R^2
$$
but since $b \leftarrow$ perturbed $\implies \nexists$ solution
thus 3.0.1.4 as a more general example

#### 3.0.1.4 Linear Regression: Linear Parameter Estimation

**Law:** $y = \alpha^\top x + \beta \quad \text{parameters} \ \alpha \in \R^n, \ \beta \in \R, \\ x \in \R^n $

**Measurements:** $(x_i, y_i) \in \R^n \times R, \ i = 1 \dots m$

**Theroretically:** (exact measurements)
$$
y_i = a^\top x_i+ \beta \qquad \text{overdetermined} \impliedby m \gt n+1 \\\,\\

\begin{bmatrix}
\bf{x_1^\top} & & 1 \\
\vdots & & \vdots \\
\bf{x_m^\top} & & 1 \\
\end{bmatrix}

\begin{bmatrix}
\bf{a} \\
\beta \\
\end{bmatrix}
=
\begin{bmatrix}
y_1 \\
\vdots \\
y_m
\end{bmatrix}
= b
$$

but since maybe $b \leftarrow$ perturbed $\implies \nexists$  solution

#### 3.0.1.6 Measuring the angles of a triangle

$$
\begin{bmatrix}
1 & 0 & 0 \\
0 & 1 & 0 \\
0 & 0 & 1\\
1 & 1 & 1 \\
\end{bmatrix}
\begin{bmatrix}
\alpha\\
\beta\\
\gamma\\
\end{bmatrix}
=
\begin{bmatrix}
\color{#bf20c9}_{\hat{\alpha}}\\
\color{#bf20c9}_{\hat{\beta}} \\
\color{#bf20c9}_{\hat{\gamma}} \\
\pi \\
\end{bmatrix}
\\
\color{#bf20c9}_{\text{measured angles}}
$$

##### Principle in Data Science
> You cannot afford, not to use every piece of information accessible!

#### 3.0.1.8 Angles in Triangulations

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/angles_in_triangulations.png)

## 3.1 Least Squares Solution Concepts

**Setting:** Overdetermined Linear Systems of Equations (OD-LSE) 
$$
Ax = b, \quad A \in \R^{m,n}, b \in \R^m \ m \gt n
$$

### 3.1.1 Least Squares Solutions: Definition

**Idea:**
> A LSQ solution of $Ax = b$ is a vector $x \in \R^n$ that minimizes 
> $$
> \color{red}{\text{residual } r \colon= b - Ax}
> $$
> with respect to $\Vert \cdot \Vert_2$

#### 3.1.1.1 Least Squares Solution
$$
A \in \R^{m,n}, \ b \in \R^m \\
x \in \R^n \leftarrow \text{LSQ.sol. of } \ Ax = b \\
\,\\
\impliedby \\
\,\\
x \in \underset{y \in \R^n}{\arg\min} \Vert Ay - b \Vert_2^2 =\colon \text{lsq}(A, b) \\
\iff\\
\Vert Ax -b \Vert_2^2 = \underset{y \in \R^n}{\min} \Vert Ay - b \Vert_2^2 = \underset{y_1, \dots, y_n \in \R}{\min} \sum_{i=1}^m \big ( \sum_{j=1}^n (A)_{i,j} y_{j} - b_i \big )^2\\
$$

##### True Generalization

$$
A \in \R^{n,n} \ \text{regular } \implies \text{lsq}(A,b) \colon= \{A^{-1}b\}
$$

#### Ex 3.1.1.5 1D Linear Regression (continuation of Ex.3.0.1.1)

OD-LSE:

$$
\begin{bmatrix}
x_1 & 1 \\
x_2 & 1 \\
\vdots & \vdots \\
x_m & 1 \\
\end{bmatrix}
\begin{bmatrix}
\alpha \\
\beta \\
\end{bmatrix}
= 
\begin{bmatrix}
y_1 \\
y_2 \\
\vdots \\
y_m \\ 
\end{bmatrix}
\leftrightarrow Ax = b, \quad A \in \R^{m,2}, \ b \in \R^m, \ x \in \R^2
$$

LSQ-Sol:
$$
\begin{bmatrix}
\alpha \\
\beta \\
\end{bmatrix}
\in \underset{\hat{\alpha}, \hat{\beta}}{\arg\min} \sum_{i=1}^m (y_i - \hat{\alpha}_i x_i- \hat{\beta})^2
$$

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/regression_line.png)

#### 3.1.1.8 (abstract) Geometric Interpretation of LSQ.Sol. of $Ax = b$

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/geometric_interpretation_of_lsq_sol.png)

$Ax^*$ is closest to $b$ in range $\mathcal{R}(A), \ x^* \colon= \text{LSQ.sol.}$
$\iff$$Ax^* = \color{red}{b_0}$ is **orthogonal projection** of $b$ onto $\mathcal{R}(A)$ 

**Geometric Intuition:**

> Least Squares Solutions always exists!
>
> $\forall A \in \R^{m,n}, \ b \in \R^m \colon \text{lsq}(A,b) \neq \empty$

### 3.1.2 Normal Equations

**Residual** $r \ \colon= b - Ax^* \perp \mathcal{R}(A) = \{Ay \ \vert \ y \in \R^n\} \\ \iff (Ay)^\top (b- Ax^*) = 0 \quad \forall y \in \R^n\\ \implies A^\top (b - Ax^*) = 0 $ 

#### Theorem 3.1.2.1 Obtaining LSQ.Sol by solving Normal Equations (NEQ)

> Vector $x \in \R^n \leftarrow$ LSQ.Sol of LSE $Ax = b, \quad A \in \R^{m,n}, b \in \R^m$
> $\iff x$ solves NEQ
> $\iff x \in \text{lsq}(A,b)$ 
> $\iff A^\top A x = A^\top b$

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/neq.png)

#### Remark 3.1.2.5 “Analysis-Style Derivation”

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/neq_analysis_style.png)

#### Ex 3.1.2.4 NEQ for Linear Regression

$$
\underbrace{
\begin{bmatrix}
\bf{x_1^\top} & 1 \\
\vdots & \vdots \\
\bf{x_m^\top} & 1 \\
\end{bmatrix}
}_A
\underbrace{
\begin{bmatrix}
\bf{a}\\
\beta \\
\end{bmatrix}
}_x
=
\begin{bmatrix}
y_1 \\
\vdots \\
y_m \\
\end{bmatrix}
\leftrightarrow
Ax = b, \quad A \in \R^{m,n+1}, \ b \in \R^m, \ x \in \R^{n+1}
$$


$$
\implies A\top A x= 

\begin{bmatrix}
X^\top X & X \cdot 1 \\
1^\top  X^\top & m = 1^\top 1
\end{bmatrix}

\begin{bmatrix}
\bf{a} \\
\beta \\
\end{bmatrix}
=
\begin{bmatrix}
Xy \\
1^\top y \\
\end{bmatrix}
$$

$m \leftarrow$ inner product of vector of $1$ with itself

##### Note:

> System Matrix of NEQ is **symmetric**

#### Theorem 3.1.2.9 Kernel & Range of $A^\top A$

$$
\mathcal{N}(A^\top A) = \mathcal{N}(A) \\
\mathcal{R}(A^\top A) = \mathcal{R}(A^\top)
$$

#### Corollary 3.1.2.13 Uniqueness of least squares solutions

> If $m \geq n$ and $\mathcal{N}(A) = \mathcal{N}(A^\top A)=\{0\}$, then the LSE $Ax = b, \ A \in \R^{m,n}, \ b \in \R^m$, has a unique LSQ.sol
> $$
> x = (A^\top A)^{-1} A^\top b
> $$
> that can be obtained by solving the normal equations
> 
> ---
>
> $$
> m \geq n \ \land \ \mathcal{N}(A) = \mathcal{N}(A^\top A) = \{0\} \implies \exists ! \ \text{LSQ.sol. of LSE} \ Ax = b, \ A \in \R^{m,n}, \ b \in \R^m \\\,\\
> x = (A^\top A)^{-1} A^\top b
> $$
> 

##### Note:

> $$
> \mathcal{N}(A^\top A) = \{0\} \implies A^\top A \ \text{regular}
> $$
>
> $$
> \mathcal{N}(A) = \{0\} \implies A^\top A \ \text{s.p.d}
> $$

##### Criterion: Full-rank Condition (FRC)
> $$
> \underset{m \gt n}{A \in \R^{m,n}} \colon \ \mathcal{N}(A) = \{0\} \iff \text{rank}(A) = n
> $$

#### Ex. 3.1.2.17 FRC for 1D Linear Regression

$$
\text{rank}
\Bigg(
\begin{bmatrix}
x_1 & 1 \\
\vdots & \vdots \\
x_m & 1 \\
\end{bmatrix}
\Bigg) = 2
\iff
\exists i,j \in \{1, \dots, m\} \colon x_i \neq x_j
$$

##### Remark

> failure of FRC 
>
> ![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/frc_failure_1d_lin_reg.png)
>
> $\implies$ flawed mathematical model
> $\implies$ useless data

### 3.1.3 Moore-penrose Pseudoinverse

LSQ.sol not unique

$\iff \text{FRC violated}$
$\iff \text{rank}(A) \lt n$
$\iff \mathcal{N}(A) \neq \{0\}$

$\implies$ additional selection criterion $\implies$ Minimal Norm Condition

#### 3.1.3.1 Generalized solution of a LSE
generalized solution $x^\dagger \in \R^n$ of LSE $Ax = b, \ A \in \R^{m,n}, b \in \R^m$ 
$$
x^\dagger \colon= \arg\min\{\Vert x \Vert_2 \ \colon \ x \in \text{lsq}(A,b) = \{y \in \R^m \ \colon \ A^\top A y = A^\top b \} \}
$$

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/visualization_of_generalized_minimal_norm_lsq_sol.png)

#### Theorem 3.1.3.6 Formula for Generalized Solution
> $A \in \R^{m,n}, b \in \R^m$, the generalized solution $x^\dagger$ of the LSE $Ax = b$
>
> $$
> x^\dagger = V (V^\top A^\top A V)^{-1} (V^\top A^\top b) = A^\dagger b
> $$
>
> where $V$ is any matrix whose columns form a basis of $\mathcal{N}(A)^\top$
>
> with the Moore-Penrose Pseudoinvariant $A^\dagger$ 

## 3.2 Normal Equation Methods

**Setting:** OD-LSE $Ax = b, \quad A \in \R^{m,n}, m \geq n, \ b \in \R^m$

**NEQ:** $A^\top Ax = A^\top b \iff \{x\} = \text{lsq}(A,b)$

**Assume:** $\text{FRC} \iff \text{rank}(A) = n \iff A^\top A \ \text{regular}$

### Algorithm 1 - Normal Equation Method for full-rank least squares problem

$$
\begin{aligned}
(i) \quad& \text{compute } \textbf{regular } \text{matrix} \ C \ \colon= A^\top A \in \R^{n,n} \\

(ii) \quad& \text{compute RHS vector} \ c \ \colon= A^\top b \\

(iii) \quad& \text{solve s.p.d LSE} \ Cx = c 
\end{aligned}
$$

**Recall S.P.D** (Def. 1.1.2.6)
$$
\begin{aligned}
\text{s.p.d} \ M \in \mathbb{K}^{n,n} &\impliedby
M = M^H \quad \land \quad \forall x \in \mathbb{K}^n \colon \ x^HMx \gt 0 \iff x \neq 0 \\
\text{s.p.s.d} \ M \in \mathbb{K}^{n,n} &\impliedby
M = M^H \quad \land \quad \forall x \in \mathbb{K}^n \colon \ x^HMx \geq 0 \iff x \neq 0
\end{aligned}
$$

```C++
// Solving the overdetermined linear system of equations
// Ax=b  by solving normal equations
// the least squares solution is returned by value
//
// Complexity:
// (i)   O(mn^2)
// (ii)  O(nm)
// (iii) O(n^3)
// 
// total: O(n^2m + n^3)
VectorXd normeqsolve(const MatrixXd &A, const VectorXd &b) {
    if (b.size() != A.rows()) throw runtime_error("Dimmension mismatch");
    // use Cholesky factorization for s.p.d. system matrix
    // M.llt() special LU-type factorization for s.p.d. matrices in O(n^3)
    VectorXd x = (A.transpose() * A).llt().solve(A.transpose() * b);
    return x;
}
```

#### Ex. 3.0.2.4 Problem with NEQ - Roundoff Errors 

$$
A = \begin{bmatrix}
1 & 1 \\
\delta & 0 \\
0 & \delta
\end{bmatrix}
\implies
A^\top A = \begin{bmatrix}
1 + \delta^2 & 1 \\
1 & 1 + \delta^2 \\
\end{bmatrix} 
\\
\delta \approx \sqrt{\text{EPS}} \implies 1 + \delta^2 = 1 \quad \color{red} \text{in} \ \mathbb{M}
$$

```C++
int main() {
	MatrixXd A(3,2);
	double eps = std::numeric_limits<double>::epsilon();
	
	A << 1, 1, sqrt(eps), 0, 0, sqrt(eps);
	// output rank of A^T A
	std::cout << "Rank of A: " << A.fullPivLu().rank() << std::endl
			  << "Rankd of A^T A: "
			  << (A.transpose() * A).fullPivLu().rank() << std::endl;
	return 0;
}
```
Output:
```bash
Rank of A: 2
Rank of A^T A: 1
```

#### Remark 3.2.0.6 Problem with NEQ for big m,n - Loss of sparsity

$A$ sparse $\not\implies A^\top A$ sparse

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/problem_neq__loss_of_sparsity.png)

trick to avoid computing $A^\top A$ $\implies$ XNEQ (extended normal equations)

#### 3.2.0.7 Extended Normal Equations 

NEQ:
$$
A^\top A x = A^\top b \iff A^\top \underbrace{(b-Ax)}_{\text{residual} \ r} = 0
$$

$$
\begin{aligned}
r &= b - Ax \\
A^\top r &= 0
\end{aligned} \Bigg\}
\implies
\underbrace{
\begin{bmatrix}
I & A \\
A^\top & 0
\end{bmatrix}}_{X} 
\begin{bmatrix}
r \\
x
\end{bmatrix}
=
\begin{bmatrix}
b \\
0
\end{bmatrix}
$$

where $X \in \R^{m+n, m+n}$
but $A$ sparse $\implies X$ sparse

## 3.3 Orthogonal Transformation Methods

**Setting:** OD-LSE $Ax = b, \ A \in \R^{m,n}, \ m \geq n$

**Assume:** FRC $\iff \text{rank}(A) = n$

**LSQ.sol:** $\{x\} = \text{lsq}(A,b) = \underset{y \in \R^{n}}{\arg\min} \Vert b - Ay \Vert_2^2$

### 3.3.1 Orthogonal Transformation Methods: Idea

with GE transform 
$$
Ax = b \to \tilde{A}x = \tilde{b} \\
$$

s.t.:
$$
\begin{aligned}
(i) \quad& \text{lsq}(\tilde{A},\tilde{b}) \leftarrow \text{easily computatble (triangular)} \\
(ii) \quad& \text{lsq}(A,b) = \text{lsq}(\tilde{A}, \tilde{b})
\end{aligned}
$$

---

$(i)$ $A$ triangular

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/orthogonal_tranformation_method_idea__easy_computability.png)

$(ii)$ Idea: $\Vert \cdot \Vert_2$-perserving (transformation) Matrix $T\in \R^{m,m}$ s.t.:
$$
\Vert Ty \Vert_2 = \Vert y \Vert_2 \quad \forall y \in \R^{m}
$$
then
$$
\underset{y \in \R^{n}}{\arg\min} \Vert Ay - b \Vert_2 = \underset{y \in \R^{n}}{\arg\min} \Vert \tilde{A}y - \tilde{b} \Vert_2 = \text{lsq}(A,b)
$$
where
$$
\tilde{A} = TA \quad \land \quad \tilde{b} = Tb
$$

### 3.3.2 Orthogonal Matrices

#### Def.: 3.3.2.1 Unitary / Orthogonal Matrices

> $$
> \begin{aligned}
> \text{unitary} \ Q \in \mathbb{K}^{n,n}, \ n \in \N \quad &\impliedby Q^H Q = I \\\,\\
> 
> \text{orthogonal} \ Q \in \R^{n,n}, \ n \in \N \quad &\impliedby Q^\top Q = I
> \end{aligned}
> $$
>
> 

### 3.3.3 QR-Decomposition

$$
\begin{aligned}
A \in \R^{m,n} \colon \quad A = QR, \quad \text{orthogonal} \ Q &\in \R^{m,m},\\
R &\in \R^{m,n}
\end{aligned}
$$



#### 3.3.3.1 QR-Decomposition: Theory

**Recall:** Gram-Schmidt Orthonormalization of $\{a^1, \dots, a^n\} \sub \R^m, m \geq n$ lin. indep.

GS:
$q^1 \colon= \frac{a^1}{\Vert a^1 \Vert_2}; % \text{1st output vector}$
$\textbf{for} \ j = 2, \dots, n \ \textbf{do}$
$\quad \textbf{for} \ l = 1, 2, \dots, j-1 \ \textbf{do}$
$\qquad q^j \leftarrow a^j - \langle a^j, q^l \rangle q^l$
$\quad \textbf{if} \ ( q^j = 0) \ \textbf{then} \ \color{red}\textbf{STOP}$
$\quad \textbf{else} \ q^j \leftarrow \frac{q^j}{\Vert q^j \Vert_2}$

#### 3.3.3.2 Span Property of G.S. Vectors

> $\{a^1, \dots a^n\} \sub \R^m$ linearly independent
> then Algorithm GS $\implies$ **orthonormal vectors** $q^1, \dots q^k \in \R^m$, s.t.:
> $$
> \text{span}\{q^1,\dots,q^l\} = \text{span}\{a^1,\dots,a^l\} \quad \forall l \in \{1, \dots, n\}
> $$
> 
---

$$
\begin{aligned}
Q &= \begin{bmatrix} q^1, \dots, q^n \end{bmatrix} \in \R^{m,n}, \quad Q^\top Q = I \\
A &= \begin{bmatrix} a^1, \dots, a^n \end{bmatrix} \in \R^{m,n}
\end{aligned}
$$

$$
\begin{aligned}
q^1 &= t_{11}a^1 \\
q^2 &= t_{12}a^1 + t_{22}a^2 \\
q^3 &= t_{13}a^1 + t_{23}a^2 + t_{33} a^3 \\
&\vdots \\
q^n &= t_{1n}a^1 + t_{2n} a^2 + \cdots + t_{nn} a^n \\
\end{aligned}
$$

$\implies \exists \ \textbf{upper triangular} \ T \in \R^{n,n} \colon \quad Q = AT$

$\text{rank}(Q) = n \quad \land \quad \text{rank}(A) = n \implies \text{rank}(T) = n$

**Note:**

> $T$ **upper triangular** $\implies T^{-1}$ **upper triangular**
>
> ![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/qr_decomp_t_inverse_upper_triangularity.png)

$$
\begin{aligned}
A &= QR \leftrightarrow 
\underbrace{
\begin{bmatrix}
&& \\
&& \\
&A& \\
&& \\
&& \\
\end{bmatrix}
}_{\text{full-rank}}
=
\underbrace{
\begin{bmatrix}
&& \\
&& \\
&Q& \\
&& \\
&& \\
\end{bmatrix}
}_{\text{orthonormal columns}}
\underbrace{
\begin{bmatrix}
&& \\
&R& \\
&& \\
\end{bmatrix}
}_{\text{upper triang., regular}} \\
\\
&\hat{=} \ \text{economical} \ QR \text{-decomposition}
\end{aligned}
$$

**Full QR-Decomposition:** (theoretical purposes only)

> extend $\{q^1, \dots, q^n\}$ to **orthonormal basis** (ONB) of $\R^n$ $\implies$ columns of **orthogonal** $\tilde{Q} \in \R^{m,m}$
>
> ![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/full_qr_decomp.png)

#### Theorem 3.3.3.4 QR-Decomposition

> $\forall A \in \mathbb{K}^{n,k} \colon \ \text{rank}(A) = k$
>
> $$
> \begin{aligned}
> (i) \quad &\exists \ \text{unique} \ Q_0 \in \R^{n,k} \colon \quad Q_0^H Q_0 = I_k \\
> &\land \ \exists \ \text{unique upper triangular} \ R_0 \in \mathbb{K}^{k,k} \colon \quad (R)_{i,i} \gt 0, \ i \in \{1, \dots, k\} \\
> &\text{s.t.:} \qquad A = Q_0 \cdot R_0 \qquad\qquad \text{economical QR-decomp} \\ \,\\
> (ii) \quad &\exists \ \text{unitary} \ Q \in \R^{n,n} \\
> &\land \ \exists \ \text{unique upper triangular} \ R \in \mathbb{K}^{n,k} \colon \quad (R)_{i,i} \gt 0, \ i \in \{1, \dots, n\} \\
> &\text{s.t.:} \qquad A = Q \cdot R \qquad\qquad \text{full QR-decomp} \\ \,\\
> (iii) \quad &\mathbb{K} = \R \implies Q_0, R_0, Q, R \leftarrow \text{real} \quad \land \quad Q \leftarrow \text{orthogonal}
> \end{aligned}
> $$
>

#### 3.3.3.2 Computation of QR-Decompositions

| Algorithm    | LA                | NMCSE                          |
| ------------ | ----------------- | ------------------------------ |
| Gram-Schmidt | orthonormal basis | unstable, affected by roundoff |

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/qr_decomp_idea_numerically_stable.png)

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/qr_decomp_idea_scheme.png)

#### 3.3.3.9 Composition of Orthogonal Transformations (OT)

> Product of two orthogonal / unitary matrices of the same size is again orthogonal / unitary

#### Ex. 3.3.3.9 2D annihilating OT

$$
a = \begin{bmatrix} a_1 \\ a_2 \end{bmatrix} \overset{Q}{\to} \begin{bmatrix} \pm \Vert a \Vert \\ 0 \end{bmatrix} \parallel e_1 \\
\,\\
Q \begin{bmatrix} a & b \end{bmatrix} = \begin{bmatrix} \ast & \ast \\ 0 & \ast \end{bmatrix}
$$

**Four Ways:**

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/2d_annihilating_OT__reflections.png) ![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/2d_annihilating_OT__rotations.png)

#### 3.3.3.10 Householder Reflections

$a, b \in \R^m, \quad \Vert a \Vert_2 = \Vert b \Vert_2$

$$
\begin{aligned}
v &\ \colon= a - b\perp b + a \\\,\\
b &= a - v \\
&= a - v \cdot \underbrace{2\frac{v^\top a}{v^\top v}}_{=1} = \underbrace{(I - 2 \frac{vv^\top}{v^\top v})}_{H(v)} a = \underbrace{H(v)}_{\text{orthogonal, symmetric}} a
\end{aligned}
$$
**Special:**  $v = a + \underbrace{\text{sgn}(a_1)}_{\text{avoids cancellation}} \Vert a \Vert_2 \ e_1$

 ![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/house_holder_reflection_apply_scheme.png)

$$
Q_{n-1}Q_{n-2}Q_{n-3}\cdots Q_1 A = R
$$

QR-Decomposition of $A \in \C^{n,n}\colon \quad A = QR \qquad \begin{cases} \text{orthogonal matrix} \ Q \ \colon= Q^H_1 \cdots Q^H_{n-1}, \\ \text{upper triangular} \ R \end{cases}$

#### Remark 3.3.3.12 HH-refl. applicable to any matrix

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/house_holder_applicability.png)

#### Remark 3.3.3.13 Encode HH Transformations

**In practice:** only $Qx$ vector needed $\implies$ sufficient to store Householder matrices

HHM: $H(v) = I - 2vv^\top \in \R^m, \quad v \in \R^m, \Vert v \Vert_2 = 1 \\ \implies \text{store only unit vector} \ v \in \R^m$ 

“to compute QR-dec.” $\hat{=}$ compute R & encode Q

#### 3.3.3.14 Givens Rotations $G_{ik}(a_i,a_k)$

**Idea:** 

> 2D rotations attack two entries of $a$ at a time

$$
\begin{bmatrix}
\gamma & \sigma \\
-\sigma & \gamma
\end{bmatrix}
\begin{bmatrix}
a_1 \\
a_k
\end{bmatrix}
=
\begin{bmatrix}
\ast \\
0
\end{bmatrix}
\,\\\,\\

G_{1k}(a_1, a_k) a \ \colon= 
\underbrace{
\begin{bmatrix}
\gamma & \cdots & \cdots & \cdots & \sigma & \cdots &\cdots & 0 \\
\vdots & 	1	& 		 &		  & \vdots & 	    && \vdots \\
\vdots & 		& \ddots & 		  & \vdots & 		&& \vdots \\
\vdots & 		& 		 & 1 	  & \vdots & 		&& \vdots \\
-\sigma & \cdots & \cdots & \cdots & \gamma & \cdots &\cdots & 0 \\
\vdots & 		& 		 &		  & \vdots &    1 && \vdots\\
\vdots & 		&		 &		  & \vdots &		&\ddots& \vdots \\
0 	   & \cdots & \cdots & \cdots & 	0  & \cdots  & \cdots& 1
\end{bmatrix}}_{\text{orthogonal}}
\begin{bmatrix}
a_1 \\
\vdots \\
\vdots \\
\vdots \\
a_k \\
\vdots \\
\vdots \\
a_n \\
\end{bmatrix}
=
\begin{bmatrix}
a_1^{(1)} \\
\vdots \\
\vdots \\
\vdots \\
0 \\
\vdots \\
\vdots \\
a_n \\
\end{bmatrix}
\quad \impliedby
\begin{cases}
\gamma = \frac{a_1}{\sqrt{\vert a_1 \vert^2 + \vert a_k \vert^2 }} \\
\sigma = \frac{a_k}{\sqrt{\vert a_1 \vert^2 + \vert a_k \vert^2 }} \\
\end{cases}
$$


$$
a \ \colon=
\begin{bmatrix}
a_1 \\
a_2 \\
\vdots \\
\vdots \\
\vdots \\
a_n
\end{bmatrix}
\overset{G_{12}(a_1,a_2)}{\longrightarrow}

\begin{bmatrix}
a_1^{(1)} \\
0 \\
a_3 \\
\vdots \\
\vdots \\
a_n
\end{bmatrix}
\overset{G_{13}(a_1^{(1)},a_3)}{\longrightarrow}

\begin{bmatrix}
a_1^{(2)} \\
0 \\
0 \\
a_4 \\
\vdots \\
a_n
\end{bmatrix}

\overset{G_{14}(a_1^{(2)},a_4)}{\longrightarrow}
\cdots
\overset{G_{1n}(a_1^{(n-2)},a_n)}{\longrightarrow}

\begin{bmatrix}
a_1^{(n-1)} \\
0 \\
\vdots \\
\\
\vdots \\
0 \\
\end{bmatrix}
$$

#### Remark 3.3.3.20 Storing Givens Transformations

$\implies$ storing two indices and rotation

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/givens_transformation_storage.png)



#### 3.3.3.22 Computational Cost

Householder-based QR-decomp. of $A \in \R^{m,n}, m \geq n$

$n-1$ steps:

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/householder_qr_dec_scheme.png)

**Note:** 
$$
H(v)\underbrace{X}_{\in \R^{m,k}} = X - v\underbrace{(v^\top X)}_{\in \R^{k}} \implies \text{cost}=O(mk) \\
\,\\
\text{cost}(\text{R-factor of} \ A \ \text{by Householder trf.}) = O(mn^2)
$$

#### Remark 3.3.3.26 QR-dec. of Banded Matrices

**Ex.:** tridiagonal matrices

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/tridiagonal_matrix.png)

Givens-QR for tridiag. matrix $\in \R^{n,n}$

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/givens_qr_tridiagonal_matrix.png)

**Note:** $R \leftarrow$ tridiagonal

**In fact:**

> $\text{cost}= O(1)$ for single rotation, because $\leq 3$ non-zero entries in every for of matrix

**In general:** bandwidth $\text{bw}(A) =$ bandwidth $\text{bw}(B)$

#### 3.3.3.4 QR-Decomposition in Eigen

```C++
#include <Eigen/QR)

// Computation of **full QR-decomposition**
// dense matrices built for both QR-factors (expensive!)
std::pair<MatrixXd, MatrixXd> qr_decomp_full(const MatrixXd &A) {
    Eigen::HouseholderQR<MatrixXd> qr(A);
    MatrixXd Q = qr.householderQ(); // multiply HHMs
    MatrixXd R = qr.matrixQR().template triangularView<Eigen::Upper>();
    
    return std::pair<MatrixXd, MatrixXd>(Q,R);
}

// Computation of **economical QR-decomposition**
// dense matrix built for Q-factor (possibly expensive!)
std::pair<MatrixXd, MatrixXd> qr_decomp_eco(const MatrixXd &A) {
    using index_t = MatrixXd::Index;
    const index_t m = A.rows(), n = A.cols();
    Eigen::HouseholderQR<MatrixXd> qr(A);
    MatrixXd Q = (qr.householderQ() * MatrixXd::Identity(m,n));
    MatrixXd R = qr.matrixQR().block(0,0,n,n).template triangularView<Eigen::Upper>();
    
    return std::pair<MatrixXd, MatrixXd>(Q,R);
}
```

**Note:**

```qr.matrixQR()``` $\in O(mn^2)$ return the compressed QR-decomposition as follows, 
where yellow columns $\implies$ vector $v$ of $H(v)$

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/eigen_matrixqr_compressed_format.png)

### 3.3.4 QR-based Solver for LSQ Problems

**Given:** 

$A \in \R^{m,n}, \ m \geq n, \quad \text{rank}(A) = n, \quad b \in \R^m \\ x \in R^n \colon \quad \Vert Ax - b \Vert_2 \to \min$

QR-dec.: $A = QR, \quad \text{orthogonal} \ Q \in \R^{m,m}, \ \implies Q^\top Q = I$
$$
\begin{aligned}
&\Vert Ax - b \Vert_2 = \Vert QRx - b \Vert_2 \\\,\\
= &\Vert Q(Rx- Q^\top b) \Vert_2 \overset{\text{orth.} \ Q}{=} \Vert Rx - Q^\top b \Vert_2 \\\,\\
\colon= &\Vert Rx - \tilde{b} \Vert_2 \ \hat{=} \ \text{triangular LSQ}
\end{aligned}
$$
**Note:** $\text{rank}(A) \implies \text{rank}(R) = n$

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/qr_based_LSQ_solver.png)

**Eigen:**

```c++
// solving full-rank LSQ ||Ax-b||->min
double lsqsolve_qr(const MatrixXd &A, const VectorXd &b, VectorXd &x) {
    x = A.householderQR().solve(b);
    return ((A*x-b).norm());
}
```

```A.householderQR()``` $\in O(mn^2)$

```.solve(b)``` $\begin{cases} Q^\top b \implies \text{application of} \ n-1 \ \text{HHMs to} \ b &\implies O(mn) \\ \text{solve LSE (backwd. elim.)} \ R_0x = (\tilde{b}_{1:n}) &\implies O(n^2) \end{cases}$

#### Normal Equations vs. OT methods

> Superior numerical stability of orthogonal transformations methods:
>
> $\bullet\quad$ Orthogonal Transformation Methods for LSQ $\impliedby \text{dense} \ A \in \R^{m,n}, \ \text{small} \ n$
>
> $\bullet\quad$ (extended) Normal Equations $\impliedby$ sparse $A \in \R^{m,n}$, big $n, m$ 

### 3.3.5 Modification Techniques for QR-decomposition

**Given:**

$\bullet\quad A \in \R^{m,n}, \ m \geq n, \ \text{rank}(A) = n$

$\bullet\quad$ (full) QR-decomp. $A = QR, \quad \text{orthogonal} \ Q \in \R^{m,m}, \ \text{upper triangular} \ R \in \R^{m,n}$

$\bullet\quad Q \leftarrow$ stored as product of $n-1$ HHMs

**Task:**

$\bullet\quad$ efficiently compute $\tilde{A} \hat{=}\ $“slight” modification of $A$

#### 3.3.5.1 Rank-1 Modification

$u \in \R^m,  \ v \in \R^n$
$$
\begin{aligned}
\tilde{A} &= A + uv^\top \\
 \tilde{A} &= QR + uv^\top = Q(R + \underbrace{Q^\top u}_{\colon= w} v^\top)
\end{aligned}
$$

##### STEP 1

compute $w \ \colon= Q^\top u \implies$ cost $O(mn)$

##### STEP 2

transform $w \to \begin{bmatrix}\ast \\ 0 \\ \vdots \\ 0\end{bmatrix} \in \R^m$ by successive Givens Transformations $\implies$ cost $=O(m)$

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/modification_techniques_qr_decomp__rank_1_mods__step2.png) 

the very right vector then is: $\Vert w \Vert_2 \ e_1$

**Note:**

> we do these transformations because of the effect on the outer product as follows:
> $$
> \begin{bmatrix}
> \ast \\
> 0 \\
> \vdots \\
> 0 \\
> \end{bmatrix}
> \begin{bmatrix}
> \ast & \cdots & \cdots & \ast
> \end{bmatrix}
> =
> \begin{bmatrix}
> \ast & \cdots & \cdots & \ast \\
> 0 & \cdots & \cdots & 0 \\
> \vdots & & & \vdots \\
> 0 & \cdots & \cdots & 0 \\
> \end{bmatrix}
> $$
> therefore we **dont destroy the upper triangualr structure of R**

meanwhile those transformations have also an effect on $R$,
namely **makeing the first subdiagonal non-zero**
which we later correct with more Givens Rotations

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/modification_techniques_qr_decomp__rank_1_mods__step2__effect_on_R.png)

**Note:** $R_1 + \Vert w \Vert_2 \ e_1 v^\top$ has the **same structure** as $R_1$

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/modification_techniques_qr_decomp__rank_1_mods__step2__effect_on_R__correction.png)

$$
\underbrace{(G_{n-1,n}G_{n-2,n-1}G_{n-3,n-2}\cdots G_{2,3}G_{1,2})}_{\colon= \ Q_2}(R_1 + \Vert w \Vert_2 \ e_1 v^\top) = \tilde{R} \implies \text{cost} = O(n^2) \\
\,\\
\tilde{A} = Q Q_1^\top Q_2^\top \tilde{R}
$$

$$
\textbf{total cost} = O(mn + n^2) \ll O(mn^2) \impliedby \text{large} \ m,n
$$

#### 3.3.5.2 Adding a Column

$A \in \R^{m,n} \to \tilde{A} = \begin{bmatrix} a_1, \dots, a_{k-1}, v, a_{k}, \dots, a_n \end{bmatrix} \in \R^{m,n+1}, \quad v \in \R^m, \ a_j \ \colon= (A)_{:,j}$

**Assume:** $m \geqq n+1$

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/modification_techniques_qr_decomp__adding_column__overview.png)

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/modification_techniques_qr_decomp__adding_column__inserting_column.png)

##### STEP 1:

compute $w \ \colon=Q^\top v \implies$ cost $=O(mn)$

##### STEP 2:

$m-n-1$ Givens Rotations $\implies$ last $m-n-1$ entries of $W$ $\mapsto$ 0

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/modification_techniques_qr_decomp__adding_column__givens_rotation_effect.png)

##### STEP 3:

bring $T$ in **upper triangular structure**  by $n-k$ Given Rotations **bottom up** $\implies$ cost $=O((n-k)^2)$
$$
T \mapsto \tilde{R} \in \R^{m,n+1}​
$$

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/modification_techniques_qr_decomp__adding_column__restoring_upper_triangularity.png)
$$
\textbf{total cost} = O(mn + (n-k)^2)
$$

#### 3.3.5.3 Adding a Row

$$
A \in \R^{m,n}, \ v \in \R^n \mapsto \tilde{A} = \begin{bmatrix}
(A)_{1,:} \\
\vdots \\
(A)_{k-1,:} \\
v^\top \\
(A)_{k,:} \\
\vdots \\
(A)_{1,:} \\
\end{bmatrix}
$$

##### STEP 1:

move new row $v$ to the bottom $\impliedby$ cyclic permutation of rows of $\tilde{A} \hat{=}$ orthogonal projection $P \in \R^{m+1,m+1}$ $\implies$ no cost, since only reindexing

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/modification_techniques_qr_decomp__adding_row__output_of_cyclic_permutation_solved_for_T.png)

##### STEP 2:

now we apply $n$ suitable Givens Rotations to restore upper triangularity of $T$ which results in $\tilde{R}$

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/modification_techniques_qr_decomp__adding_row__restore_upper_triangularity.png)

$$
\textbf{total cost} = O(n^2)
$$

## 3.4 Singular Value Decomposition (SVD)

### 3.4.1 SVD: Definition and Theory

#### 3.4.1.1 (full) Singular Value Decomposition

$\forall A \in \mathbb{K}^{m,n} \\ 
\quad \exists \ \text{unitary / orthogonal} \ U \in \mathbb{K}^{m,m}, V \in \mathbb{K}^{n,n} \ \land \ \exists \ \text{diagonal} \ \Sigma = \text{diag}(\sigma, \dots, \sigma_p) \in \R^{m,n} \\
\quad \land \ p \ \colon= \min\{m,n\} \quad \land \underbrace{\sigma_1 \geq \sigma_2 \geq \dots \geq \sigma_p}_{\text{singular values}} \geq 0 \ \colon$
$$
A = U\Sigma V^H
$$


for $m \geq n$

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/full_svd__m_geq_n.png)

for $n \gt m$

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/full_svd__n_geq_m.png)

#### Lemma 3.4.1.6 

> The **squares of the non-zero singular values** $\sigma_i^2$ of $A$
> are the **non-zero eigenvalues** of $A^H A$ and $AA^H$
> with **associated eigenvectors** $(V)_{:,1},\dots,(V)_{:,p}$ and $(U)_{:,1},\dots,(U)_{:,p}$ respecively

$$
\begin{aligned}
\color{orange} A^H A &\overset{\tiny\text{SVD}}{=} (U\Sigma V^H)^H (U \Sigma V^H) \\
&= V\Sigma^H U^H U \Sigma V^H \\
&= V \textcolor{orange}{\Sigma^H  \Sigma} V^H \\
\end{aligned}
$$

where $\textcolor{orange}{\text{orange}}$ denotes similar matrices with the same eigenvalues

**Note:** $m \gt n \implies m-n$ right columns of $U$ irrelevant

##### 3.4.1.1.1 (economical) Singluar Value Decomposition

$m \gt n \implies$ ![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/eco_svd__m_gt_n.png)

$n \gt m \implies$ ![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/eco_svd__n_gt_m.png) 
$\implies n-m$ right columns of $V$ irrelevant

#### Remark 3.4.1.7 SVD = Additive Rank-1 Decomposition

$p \ \colon = \min\{m,n\} \\
U = \begin{bmatrix} u_i \end{bmatrix}_{i=1}^m \\
V = \begin{bmatrix} v_i \end{bmatrix}_{i=1}^n$
$$
A  = U \Sigma V^H = \sum_{j = 1}^p \sigma_j \underbrace{u_j v_j^H}_{m \times n\ \text{rank} \ 1}
$$
**Assume:** $\sigma_{r+1} = \dots = \sigma_p = 0$
$$
\begin{aligned}
A &= U \Sigma V^H= \sum_{j=1}^r \sigma_j u_j v_j^H \\
A v_l &= \sum_{j=1}^r \sigma_j \underbrace{u_j v_j^H}_{\delta_{jl}} = 
\begin{cases} 
\sigma_l u_l & \impliedby l \leq r \\
0 & \impliedby l \gt r
\end{cases} \\
\,\\
\implies \mathcal{R}(A) &= \text{span}\{u_1, \dots, u_r\} \implies \text{rank}(A) = r \\
\implies \mathcal{N}(A) &= \text{span}\{v_{r+1}, \dots, v_n\}
\end{aligned}
$$

#### Lemma 3.4.1.14 SVD and Rank of Matrices

> $\exists \ r \in [1, p \ \colon= \min\{m,n\}] \ \colon \\ 
>  \quad \sigma_1 \geq \dots \geq \sigma_r \gt \sigma_{r+1} = \dots = \sigma_p = 0$
> $$
> \begin{aligned}
> (i) \quad &\text{rank}(A) = r = \# \text{non-zero singular values} \\
> (ii) \quad &\mathcal{N}(A) = \text{span}\{(V)_{:,r+1}, \dots, (V)_{:,n}\} \\
> (iii) \quad &\mathcal{R}(A) = \text{span}\{(U)_{:,1}, \dots, (U)_{:,r}\}
> \end{aligned}
> $$

### 3.4.2 SVD in Eigen

```c++
#include <Eigen/SVD>

// computation of **full SVD A = USV^H
// SVD factors are returned as dense matrices in natural order
std::tuple<MatrixXd, MatrixXd, MatrixXd> svd_full(const MatrixXd &A) {
    Eigen::JacobiSVD<MatrixXd> svd(A, Eigen::ComputeFullU | Eigen::ComputeFullV);
    MatrixXd U = svd.matrixU(); // get unitary (square) matrix U
    MatrixXd V = svd.matrixV(); // get unitary (square) matrix V
    VectorXd sv = svd.singularValues(); // get singular value vector (decr. order)
    MatrixXd Sigma = MatrixXd::Zero(A.rows() * A.cols());
    const unsigned p = sv.size(); // # singular values
    Sigma.block(0,0,p,p) = sv.asDiagonal(); // set diagonal block of S
    
    return std::tuple<MatrixXd, MatrixXd, MatrixXd>(U,Sigma,V);
}

// computation of **economical (thin) SVD A = USV^H
// SVD factors are returned as dense matrices in natural order
//
// cost(eco SVD of A in K^{m,n}) = O(min{m,n}^2 * max{m,n})
std::tuple<MatrixXd, MatrixXd, MatrixXd> svd_eco(const MatrixXd &A) {
    Eigen::JacobiSVD<MatrixXd> svd(A, Eigen::ComputeThinU | Eigen::ComputeThinV);
    MatrixXd U = svd.matrixU(); // get matrix U with orthonormal columns
    MatrixXd V = svd.matrixU(); // get matrix V with orthonormal columns
    VectorXd sv = svd.singularValues(); // get singular value vector (decr. order)
    MatrixXd Sigma = sv.asDiagonal(); // build diagonal matrix
}
```

**Note:**

> Singular Values are **affected by roundoff**

#### Ex 3.4.2.3 SVD basesd computation of Rank

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/computation_of_rank_through_svd.png)

```C++
// compuation of the numerical rank of a matrix by means of SVD
MatrixXd::Index rank_eigen(const MatrixXd &A, double tolerance = EPS) {
    return A.jacobiSvd().setThreshold(tolerance).rank();
}
```

#### Ex 3.4.2.7 Computation of $\mathcal{N}(A)$ / $\mathcal{R}(A)$

$\implies$ provide basis for $\mathcal{N}(A)$ / $\mathcal{R}(A)$

```C++
// computation of an **ONB of the kernel** of a matrix
MatrixXd nullspace(const &A, double tolerance = EPS) {
    using index_t = MatrixXd::Index;
    Eigen::JacobiSVD<MatrixXd> svd(A, Eigen::ComputeFullV);
    index_t r = svd.setThreshold(tolerance).rank();
    // rightmost columns of V provide ONV of N(A)
    MatrixXd ONB = svd.matrixV().rightCols(A.cols() - r);
    return ONB;
}

// computation of an **ONB of the image space** of a matrix
MatrixXd nullspace(const &A, double tolerance = EPS) {
    using index_t = MatrixXd::Index;
    Eigen::JacobiSVD<MatrixXd> svd(A, Eigen::ComputeFullU);
    index_t r = svd.setThreshold(tolerance).rank();
    // r left columns of U provide ONV of R(A)
    MatrixXd ONB = svd.matrixU().leftCols(r);
    return ONB;
}
```

### 3.4.3 Solving General LSQ Problems by SVD

**Given:**

$A \in \R^{m,n}, \ m,n \in \N, \quad b \in \R^m \quad \text{rank}(A) \leq \min\{m,n\}$

**Find:**

$x^\dagger \in\R^n \colon \quad$
$$
\begin{aligned}
(i) \quad & \Vert Ax^\dagger -b \Vert_2 = \min \{\Vert Ay - b \Vert_2 \ \colon \ y \in \R^n\} \iff x^\dagger \in \text{lsq}(A,b)\\
(ii) \quad & \Vert x^\dagger \Vert \overset{(i)}{=} \min
\end{aligned}
$$

$$
A = \begin{bmatrix} U_1 & U_2\end{bmatrix} \begin{bmatrix} \Sigma_r & 0 \\ 0 & 0 \end{bmatrix} \begin{bmatrix} V_1^\top \\ V_2^\top \end{bmatrix}
$$

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/general_LSQ_by_SVD__problem_scheme.png)

$\text{Lemma 3.4.1.13} \implies \text{orthogonal} \ V \in \R^{n,n}$

$$
\begin{aligned}
\mathcal{N}(A)^\top &= \text{span}\{v_1, \dots v_r\} \\
&= \mathcal{R}(V_1) = \{V_1 y \ \colon \ y \in \R^r\} \\
\,\\
x^\dagger &= V_1 y^\dagger \qquad y^\dagger \in \R^\dagger \qquad\qquad\qquad\quad\ \ (1) \\
\,\\
\text{lsq}(A,b) &= \{x \in \R^n \ \colon A^\top A x = A^\top b\} \qquad\qquad\,(2)
\end{aligned}
$$

$$
\begin{aligned}
(1) \ \land \ (2) \implies A^\top A V_1 y^\dagger &= A^\top b \\
V_1^\top A^\top A V_1 y^\dagger &= V_1^\top A^\top b \\
A = U \Sigma V^\top \implies V_1^\top V \Sigma^\top U^\top U \Sigma V^\top V_1 y^\dagger &= V_1^\top V \Sigma^\top U^\top b \\
V_1^\top V \Sigma^\top \Sigma V^\top V_1 y^\dagger &= V_1^\top V \Sigma^\top U^\top b \\
V_1^\top 
\underbrace{
\begin{bmatrix} V_1 & V_2 \end{bmatrix}
}_{\begin{bmatrix} I & 0 \end{bmatrix}}
\underbrace{
\begin{bmatrix} \Sigma_r & 0 \\ 0 & 0 \end{bmatrix} 
\begin{bmatrix} \Sigma_r & 0 \\ 0 & 0 \end{bmatrix}
}_{\begin{bmatrix} \Sigma_r^2 & 0 \\ 0 & 0 \end{bmatrix}}
\underbrace{
\begin{bmatrix} V_1^\top \\ V_2^\top \end{bmatrix} 
V_1
}_{
\begin{bmatrix}
I \\
0 \\
\end{bmatrix}
}
\ y^\dagger &= V_1^\top
\begin{bmatrix} V_1 & V_2 \end{bmatrix}
\begin{bmatrix} \Sigma_r & 0 \\ 0 & 0 \end{bmatrix} 
\begin{bmatrix} V_1^\top \\ V_2^\top \end{bmatrix} 
b \\
\Sigma_r^2 y^\dagger &= \Sigma_r U_1^\top b \\
\Sigma_r y^\dagger &= U_1^\top b \\
\text{regular} \ \Sigma_r \in \R^{r,r} \implies y^\dagger &= \Sigma_r^{-1} b \\
\,\\
\implies x^\dagger &= V_1 \Sigma_r^{-1} U_1^\top b \\
x^\dagger &= A^\dagger b
\end{aligned}
$$

```C++
#include <Eigen/SVD>

VectorXd lsq_svd(const MatrixXd &A, const VectorXd &b) {
    // compute economical SVD
    Eigen::JacobiSVD<MatrixXd> svd(A, Eigen::ComputeThinU | Eigen::ComputeThinV);
    VectorXd sv = svd.singularValues();
    unsigned int r = svd.rank(); // numerical rank, default tolerance of EPS
    MatrixXd U = svd.matrixU();
    MatrixXd V = svd.matrixV();
    // x^+ = V_1 S_r^(-1) U_1^H b
    return V.leftCols(r) * (sv.head(r).cwiseInverse().asDiagonal() * (U.leftCols(r).adjoint() * b));
}

VectorXd lsq_svd_eigen(const MatrixXd &A, const VectorXd &b) {
    Eigen::JacobiSVD<MatrixXd> svd(A, Eigen::ComputeThinU | Eigen::ComputeThinV);
    return svd.solve(b);
}
```

#### Theorem 3.4.3.16 Pseudoinverse and SVD

> $A \in \mathbb{K}^{m,n} \ \exists \ \text{SVD-decomp.} \ \colon \quad A = U \Sigma V^H \ \text{partitioned as in picture (3.4.3.1)} \\
> \implies \text{Moore-Penrose Pseudoinverse} \ A^\dagger = V_1 \Sigma_r^{-1}U_1^H$

### 3.4.4 SVD-based Optimization and Approximation

#### 3.4.4.1 Norm-Constrained Extrema of Quadratic Forms

**Given:** $A \in \mathbb{K}^{m,n}, \ m \geq n$

**Find:** $x \in \mathbb{K}^n, \ \Vert x \Vert_2 = 1, \quad \Vert Ax\Vert_2 \to \text{goal} \in\{\min, \max\}$
$$
\begin{aligned}
\text{eco-SVD} \implies A &= U \Sigma V^\top \\
\Vert Ax \Vert_2 &= \Vert U \Sigma V^\top x \Vert_2 \\
\text{orthogonal} \ U \implies &=  \Vert \Sigma V^\top x \Vert_2 \\ \,\\

y \ \colon= V^\top x \implies \Vert Ax \Vert_2^2 &= \Vert \Sigma y \Vert_2^2 \\
&= \sum_{l=1}^n \sigma_l^2 y_l^2 \to \text{goal} \in \{\min, \max\} \\ \,\\

\begin{Bmatrix}
\text{descending order of} \ \Sigma \\
\land \ \text{orthogonality of} \ V^\top \\
\land \ \text{constraint} \ \Vert x \Vert_2 = 1 \\
\implies \Vert V^\top x \Vert_2 = \Vert y \Vert_2 = 1 
\end{Bmatrix}

\implies &= \sum_{l=1}^n \sigma_l^2 y_l^2 \to \begin{cases}
\max & \implies y = \begin{bmatrix} 1 & 0 & \cdots & 0 \end{bmatrix}^\top = e_1 \\
\min & \implies y = \begin{bmatrix} 0 & \cdots & 0 & 1 \end{bmatrix}^\top = e_n \\
\end{cases} \\ \,\\

y = V^\top x \quad &
\begin{cases}
\max &\implies x = V e_1 = (V)_{:,1} \\
\min &\implies x = V e_n = (V)_{:,n} \\
\end{cases} \\ \,\\

& \begin{cases}
\max &\implies \Vert Ax \Vert_2 = \Vert A (V)_{:,1} \Vert_2 = \sigma_1 \\
\min &\implies \Vert Ax \Vert_2 = \Vert A (V)_{:,n} \Vert_2 = \sigma_n \\
\end{cases} \\ \,\\
\end{aligned}
$$

**Euclidean Matrix Norm**
$$
\Vert A \Vert_2 \ \colon= \max_{\Vert x \Vert = 1} \Vert Ax \Vert = \sigma_1
$$

#### 3.4.4.2 Best Low-Rank Approximation

**Context:**
Matrix compression by $\color{red}\text{data sparse}$ matrices
$$
r \ \colon = \text{rank}(A \in \R^{m,n}) \\
\text{SVD} \implies A = \sum_{k=1}^r \sigma_k (U)_{:,k} (V)_{:,k}^\top \\
\,\\
\text{storage} \approx r(m+n) \ll mn
$$

#### Theorem 3.4.4.18 Best Low-Rank Approximation

> SVD $A = U \Sigma V^H$ of $A \in \mathbb{K}^{m,n}$
> $$
> A_k \ \colon = \underbrace{U_k \Sigma_k V_k^h}_{\text{truncated SVD}} = \sum_{l=1}^k \sigma_l (U)_{:,l} (V)_{:,l}^\top \\ \,\\ 
> 
> \text{with} \quad
> 
> \begin{aligned} 
> U_k \ &\colon= \begin{bmatrix} (U)_{:,1}, \dots, (U)_{:,k} \end{bmatrix} \in \mathbb{K}^{m,k} \\
> V_k \ &\colon= \begin{bmatrix} (V)_{:,1}, \dots, (V)_{:,k} \end{bmatrix} \in \mathbb{K}^{n,k} \\
> \Sigma_k \ &\colon= \text{diag}(\sigma_1, \dots, \sigma_k) \in \mathbb{K}^{k,k} \\
> \end{aligned}
\quad
> \text{for} \quad 1 \leq k \leq \text{rank}(A)
> $$
> then for $\Vert \cdot \Vert = \Vert \cdot \Vert_2$ and $\Vert \> cdot \Vert = \Vert \cdot \Vert_F$
> $$
> \Vert A - A_k \Vert \leq \Vert A - F \Vert \quad \forall F \in \mathcal{R}_k(m,n)
> $$
> holds true. Where $\mathcal{R}_k(m,n) = \{M \in \mathbb{K}^{m,n} \colon \text{rank}(M) = k\}$.
> 
> Therefore $A_k$ is the **best rank $k$ approximation** of $A$ in the above matrix norms

**Approximation Error Norm:**

$(m \geq n)$
$$
\begin{aligned}
\text{full SVD} \ \colon \quad A &= U \Sigma V^H = U 

\left[
\begin{array}{c|c}
\Sigma_k & 0 \\
\hline \\
0 & \small\begin{matrix} \sigma_{k+1} & &  \\ & \ddots & \\ & & \sigma_n \end{matrix} \\
\hline \\
0 & 0
\end{array}
\right]

V^H \\

A_k &= U 
\left[\begin{array}{c|c}
\Sigma_k & 0 \\
\hline
0 & 0 \\
\end{array}\right]
V^H \\ \,\\

\Vert A - A_k \Vert_{2 | F} &= 

\begin{Vmatrix}
U
\left[\begin{array}{c|c}
0 & 0 \\
\hline \\
0 & \small\begin{matrix} \sigma_{k+1} & &  \\ & \ddots & \\ & & \sigma_n \end{matrix} \\
\hline \\
0 & 0 \\
\end{array}\right]
V^H
\end{Vmatrix}_{2 | F} \\

&= 
\begin{Vmatrix}
\left[\begin{array}{c|c}
0 & 0 \\
\hline \\
0 & \small\begin{matrix} \sigma_{k+1} & &  \\ & \ddots & \\ & & \sigma_n \end{matrix} \\
\hline \\
0 & 0 \\
\end{array}\right]
\end{Vmatrix}_{2|F}
= \begin{cases} \Vert \cdot \Vert_2 & \implies \sigma_{k+1} \\ \Vert \cdot \Vert_F & \implies \sigma_{k+1}^2 + \dots + \sigma_n^2 \end{cases}
\end{aligned}
$$
**Note:**

> **decay of the singular values**
> **predicts quality of low-rank approximation** 

**Relative Compression Error**
$$
\frac{\Vert A - A_K \Vert_2}{\Vert A \Vert} = \frac{\sigma_{k+1}}{\sigma_1} \stackrel{!}{\leq} \text{tolerance}
$$

#### Ex. 3.4.4.23 SVD for image compression

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/svd_image_compression)

#### 3.4.4.16 Frobenius Norm
$A \in \mathbb{K}^{m,n}$
$$
\begin{aligned}
\Vert A \Vert_F^2 \ &\colon= \sum_{i=1}^m\sum_{j=1}^n \vert a_{ij} \vert ^2 \\
&\hat{=} \ \text{euclidean vector norm for matrices}
\end{aligned}
$$

#### 3.4.4.5 Fitting of Hyperplanes (in Hesse Normal Form)

normal vector of length $\Vert n \Vert_2 = 1, \quad c \in \R$

Hyperplane $\mathcal{H} \ \colon=\{x \in \R^d \colon \quad c + n^\top x = 0\}$

**Given:**

 point coordinate vectors $y_1, \dots, y_m \in \R^d, \ m \gt d$

**Find:**

Hyperplane $\mathcal{H} \leftrightarrow\{c \in \R, \ n \in \R^d, \ \Vert n \Vert_2 = 1\}$, such that:
$$
\begin{aligned}
\sum_{j=1}^m \text{dist}(\mathcal{H}, y_j)^2 &= \sum_{j=1}^m \vert c + n^\top y_j \vert^2 \to \min \\ \,\\

&\iff
\begin{Vmatrix}
\underbrace{
\begin{bmatrix}
1 & y_{1,1} & \cdots & y_{1,d} \\
\vdots & y_{2,1} & \cdots & y_{2,d} \\
\vdots & \vdots & & \vdots \\
1 & y_{1,1} & \cdots & y_{1,d} \\
\end{bmatrix}
}_{=\colon A}
\underbrace{
\begin{bmatrix}
c \\
n_1 \\
\vdots \\
n_d \\
\end{bmatrix}
}_{=\colon x}
\end{Vmatrix}_2
\to \min
\end{aligned}
$$
**Note:** $c$ is not subject to constraint
$$
A \ \colon=
\begin{bmatrix}
1 & y_{1,1} & \cdots & y_{1,d} \\
\vdots & y_{2,1} & \cdots & y_{2,d} \\
\vdots & \vdots & & \vdots \\
1 & y_{1,1} & \cdots & y_{1,d} \\
\end{bmatrix}
= QR \\ \,\\

R \ \colon=
\begin{bmatrix}
r_{1,1} & r_{1,2} & \cdots & \cdots & r_{1,d+1}  \\
0 & r_{2,2} & \cdots & \cdots & r_{2,d+1}  \\
\vdots & & \ddots & & \vdots \\
\vdots & & & \ddots & \vdots \\
0 & \cdots & \cdots& 0 & r_{d+1, d+1} \\
0 & \cdots & \cdots& \cdots &0  \\
\vdots & & & & \vdots \\
0 & \cdots & \cdots& \cdots &0  \\
\end{bmatrix}
\in \R^{m,d+1} \\ \,\\\,\\

\Vert Ax \Vert_2 = \Vert QRx \Vert_2 = \Vert Rx \Vert_2 \\ \,\\

\begin{aligned}
\Vert Ax \Vert_2 \to \min &\iff \Vert Rx \Vert_2 \to \min \\

&\iff \begin{Vmatrix}
\begin{bmatrix}
r_{1,1} & r_{1,2} & \cdots & \cdots & r_{1,d+1}  \\
0 & r_{2,2} & \cdots & \cdots & r_{2,d+1}  \\
\vdots & & \ddots & & \vdots \\
\vdots & & & \ddots & \vdots \\
0 & \cdots & \cdots& 0 & r_{d+1, d+1} \\
0 & \cdots & \cdots& \cdots &0  \\
\vdots & & & & \vdots \\
0 & \cdots & \cdots& \cdots &0  \\
\end{bmatrix}

\begin{bmatrix}
c \\
n_1 \\
\vdots \\
n_d \\
\end{bmatrix}
\end{Vmatrix}_2 \to \min \\

& \iff \begin{Vmatrix}
\begin{bmatrix}
r_{1,1} & \cdots & \cdots & r_{1,d+1}  \\
0 & & & & & \\
\vdots & & \tilde{A} & & \\
0 & & & & \\

\end{bmatrix}
\begin{bmatrix}
c \\
n_1 \\
\vdots \\
n_d \\
\end{bmatrix}
\end{Vmatrix}_2 \to \min \\

\end{aligned}
$$

$$
\begin{aligned}
(i) \quad & n \in \R^d, \ \Vert n \Vert_2 = 1  \colon \quad \Vert \tilde{A}n \Vert_2\to \min \\
(ii) \quad & c \ \colon= -\frac{1}{r_{1,1}} \sum_{l = 1}^d n_l r_{1,l} \ \implies (Ax)_1 = 0
\end{aligned}
$$

**Generalized distance fitting of a hyperplane**

```C++
// fitting of a hyperplane,
// returning Hesse Normal Form
void clsq(const MatrixXd &A, const unsigned dim, double &c, VectorXd &n) {
    unsigned p = A.cols(), m = A.rows();
    if (p < dim + 1) { cerr << "not enough unknowns\n"; return; }
    if (m < dim) { cerr << "not enough equations\n"; return; }
    
    m = std::min(m, p);
    // first step:	orthogonal transformation
    MatrixXd R = A.householderQR().matrixQR().template triangularView<Eigen::Upper>();
    // compute matrix V from SVD composition of R
    MatrixXd V = R.block(p - dim, p - dim, m + dim - p, dim) // A_tilde
        			.jacobiSvd(Eigen::ComputeFullV).matrixV();
    n = V.col(dim-1);
    
    MatrixXd R_topleft = R.topLeftCorner(p - dim, p - dim);
    c = -(R.topLeft.template triangularView<Eigen::Upper>()
          	.solve(R.block(0, p - dim, p - dim, dim)) * n)(0);
}
```

$$
A \in \R^{m,p} \ \colon \ \text{solves} \ \begin{Vmatrix} A \begin{bmatrix} c \\ n \end{bmatrix}\end{Vmatrix}_2 \to \min \\
n \in \R^d, \ \Vert n \Vert_2 = 1
$$

#### 3.4.4.3 Principal Component Analysis (PCA)

> useful for **trend detection** and **data classification**

#### Ex. 3.4.4.25 Trend Analysis

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/pca__trend_analysis_stock_prices.png)

prices of $n$ stocks for $m$ days
$$
a_k \in \R^m, \quad k \in \{1, \dots, n\} \\
A = \begin{bmatrix} a_1, \dots, a_n \end{bmatrix} \in \R^{m,n}
$$
**Task:**

> **Data Science:** 
>
> identify dominant / **uncorrelated** trends
>
> **Linear Algebra (unrealistic):**
>
> find **orthonormal** vectors $u_1, \dots u_p \in \R^m, \ p \ll m,n$ such that
> $$
> a_k \in \text{span}\{u_1, \dots, u_p\}
> $$
> though its not realistic due to **random perturbations**
>
> **Linear Algebra (realistic):**
>
> find **orthonormal** vectors $u_1, \dots, u_p \in \R^m, \ p \ll m,n$ such that
> $$
> a_k \in \text{span}\{u_1, \dots, u_p\} + \color{violet}\text{small remainder} \\ \,\\
> \text{SVD:} \quad  A = \sum_{l=1}^p \sigma_l u_l v_l^\top + \color{violet} \sum_{l=p+1}^{\min\{m,n\}} \sigma_l u_l v_l^\top \\
> [A = U \Sigma V^\top \colon \quad u_l \ \colon= (U)_{:,l}, \ v \ \colon= (V)_{:,l}] \\
> $$
> this is **achievable** $\impliedby$**fast (exponential) decay of singular values** because
> $$
> \begin{Vmatrix} \color{violet} \sum_{l=p+1}^{\min\{m,n\}} \sigma_l u_l v_l^\top \end{Vmatrix} = \sigma_{p+1}
> $$
> **Terminology:**
>
> | Data Science                      | Linear Algebra               |
> | --------------------------------- | ---------------------------- |
> | trends                            | $p$ leftmost columns of $U$  |
> | importance of $l$-th trend        | $\sigma_l$                   |
> | strength of $l$-th trend in $a_k$ | entry $k$ of column $l$ of V |
>
> ![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/pca_streng_of_lth_trend_in_ak.png)

#### Ex 3.4.4.25 / 3.4.4.30 PCA Stock Prices

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/pca_stock_prices_singular_values.png)

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/pca_5_most_important_trends.png)

#### 3.4.4.33 Proper Orthogonal Decomposition (POD)

**Given:**

Data Points $a_1, \dots, a_n \in \R^m, \ m,n \in \N$ often $n \gg m$

**Sought:**

for $k \leq \min\{m,n\}$ find **subspace** $U_k \sub \R^m$ ($\implies$provide **ONB**) such that
$$
\begin{aligned}
U_k &= \underset{W \sub R^m, \ \text{dim} W= k}{\arg\min} \sum_{j=1}^n \text{dist}(a-j, W) \\
&= \underset{W \sub R^m, \ \text{dim} W= k}{\arg\min} \sum_{j=1}^n \inf_{w \in W}\Vert a_j - w \Vert_2^2
\end{aligned}
$$
**1D Linear Regress vs. 2D POD**

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/difference_1d_linReg__2d_POD.png)

Subspace $W \sub \R^m, \ \text{dim}W=k$, 
ONB: $W = \text{span}\{w_1, \dots, w_k\}$
$$
\begin{aligned}
W &= \begin{bmatrix} w_1, \dots, w_k \end{bmatrix} \in \R^{m,k}, \ k \leq m \\ \,\\
\text{dist}(x,W) &= \min_{y \in \R^k} \Vert x - Wy \Vert_2 \\
&\ \hat{=} \ \text{LSQ Problem, minimize} \ y = W^\top x \\
&= \Vert x - WW^\top x\Vert_2 \\\,\\

\sum_{j=1}^n \inf_{w \in W} \Vert a_j - w \Vert_2^2 &= \sum_{j=1}^n \inf_{w \in W} \Vert a_j - WW^\top a_j \Vert_2^2 = \Vert A - WW^\top A \Vert_F^2 \\ \,\\

\text{POD} &\leftrightarrow \underset{W \in R^{m,k}, W^\top W = I}{\arg\min} \Vert A - \underbrace{WW^\top A}_{\text{rank} \leq k} \Vert_F^2 \\\,\\

\text{best low-rank approx.} \implies \Vert A - WW^\top A \Vert_F &\geq \Vert A - U_k \Sigma_k V_k \Vert_F \quad \forall W\in \R^{m,k}, \ WW^\top = I
\end{aligned}
$$
therefore we need to find $W \in \R^{m,k}$ with
$$
\begin{aligned}
(i) \quad & W^\top W = I \\
(ii) \quad & WW^\top A = U_k \Sigma_k V_k
\end{aligned}
$$
with $W = U_k$
$$
WW^\top A = U_k U_k^\top U \Sigma V^H \underbrace{=}_{\tiny\text{orthonormal columns of} \ U} U_k \begin{bmatrix} I_k & 0 \end{bmatrix} \Sigma V^\top = U_k \Sigma_k V_k^\top
$$

#### Theorem 3.4.4.47 Solution of POD

> Subspace $U_k$ spanned by the first $k$ left singular vectors of $A = \begin{bmatrix} a_1, \dots, a_k \end{bmatrix} \in \R^{m,n}$
> solves the POD Problem
> $$
> U_k = \mathcal{R}((U)_{:,1:k}) \quad \text{with} \quad A= U \Sigma V^\top \quad \text{the SVD of} \ A
> $$

**POD Estimate:**
$$
\sum_{j=1}^n \inf_{w \in U_k} \Vert a_j -w \Vert_2^2= \sum_{j=k+1}^p \sigma_l^2
$$


#### Ex 3.4.4.32 Detect Data Points $\in$ Subspace

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/pod__data_detection_of_subspace.png)

SVD detects id data approximately $\in$ subspace

#### Ex 3.4.4.26 Data Classification

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/svd_pod_data_classification_ex_problem.png)

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/svd_pod_data_classification_visualized_data.png)

two trends $\iff$ two types
data vectors $a_k\in \R^m$, namely $m$ voltages
$$
\begin{aligned}
A &= \begin{bmatrix}a_i, \dots, a_n \end{bmatrix} \in \R^{m,n} \\ 
n \ &\hat{=} \ \# \text{diodes}
\end{aligned}
$$
![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/svd_pod_data_classification_sv.png)

two dominant singular values

**Idea:**

> Classification by **comparing components of $(V)_{:,1}$ and $(V)_{:,2}$**
> $(V)_{:,1}$ / $(V)_{:,2}$ $\implies$ **strength of presence of trends**  $(U)_{:,1}$ / $(U)_{:,2}$ in a particular measurement

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/svd_pod_data_classification_streng_of_presence_of_trends_comparison.png)

## 3.6 Constrained Least Squares

$m \times n$ OD-LSE, but $p \lt n$ equations have to be satisfied exactly
$$
\begin{matrix}
\begin{matrix}
m & \updownarrow \\
p & \updownarrow \\
\end{matrix}
&
\begin{bmatrix}
A \\
C \\
\end{bmatrix}
x = 
\begin{bmatrix}
b \\
d \\
\end{bmatrix}
, \quad m \geq n
\end{matrix}
$$
**Given:**

$A \in \R^{m,n}, \ m \geq n, \ \text{rank}(A) = n, \ b = \R^m$
$C \in \R^{p,n}, \ p \lt n, \ \text{rank}(C) = p, \ d \in \R^p$

**Find:**
$$
x \in \R^n \colon \qquad\qquad \Vert Ax -b \Vert_2 \to \min \quad \land \quad \underbrace{Cx = d}_{\textbf{linear } \text{constraint}}
$$

### 3.6.1 Solution via Lagrange Multipliers

**Idea:** couple constraint using the **Lagrange multiplier** $m \in \R^p$
$$
\begin{aligned}
x &= \underset{y \in \R^n}{\arg\min} \sup_{m \in \R^p} L(y,m) \qquad \qquad \color{violet}\text{(saddle-point problem)} \\\,\\
\underbrace{L(y,m)}_{\begin{matrix} \text{Lagrange Functional} \\= \ \infty \iff Cy-d \neq 0\end{matrix}} \ \colon&= \frac{1}{2} \Vert Ay -b \Vert_2^2 + m^\top (Cy -d)
\end{aligned}
$$

#### 3.6.1.2 Saddle-Point Problem

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/constrained_LSQ__saddle_point_problem.png)
$$
\text{Graph of} \ L \ \text{is flat (aka all partial derivatives vanish) in the saddle point} \\\,\\
L(x,m) = x^2 - 2xm \\
[n = p = 1]
$$
Saddle point $(x,q)$ of $L$
$$
\implies \frac{dL}{dy}(x,q) = \frac{dL}{dm}(x,q) = 0
$$
computing partial derivatives $\implies$ quadratic polynomial in $y_i, m_k$
$$
\begin{aligned}
L(y,m) &= -\frac{1}{2} \sum_{j=1}^m \bigg( \sum_{i=1}^n (A)_{j,i} y_i - b_j \bigg)^2 \\
& \qquad +\sum_{k=1}^p m_k \bigg( \sum_{l=1}^n (C)_{k,l} y_l - d_k \bigg) \\
\end{aligned} \\ \,\\
y = \begin{bmatrix} y_1, \dots, y_n \end{bmatrix}^\top, \quad m = \begin{bmatrix} m_1, \dots, m_p \end{bmatrix}^\top
$$

$$
\textcolor{violet}{\frac{dL}{dy_1}(x,q) = \dots = \frac{dL}{dy_n}(x,q)} 
= 
\textcolor{#23aaff}{\frac{dL}{dm_1}(x,q) = \dots = \frac{dL}{dm_p}(x,q)} = 0 \\\,\\
\,\\
\textcolor{violet}{\frac{dL}{dy}(x,q) = A^\top (Ax-b)+C^\top q \stackrel{!}{=}0} \\
\textcolor{#23aaff}{\frac{dL}{dy}(x,q) =  Cx-d \stackrel{!}{=} 0} \\
\,\\\,\\

(n+p) \times (n+p) \ \text{LSE} \qquad \underbrace{
\begin{bmatrix}
A^\top A & C^\top \\
C & 0
\end{bmatrix}
}_{\text{symmetric}}
\begin{bmatrix}
x \\
q \\
\end{bmatrix}
=
\begin{bmatrix}
A^\top b \\
d
\end{bmatrix}

\qquad \begin{matrix}
\text{Augmented Normal Equations}\\
\text{(matrix saddle point problem)}
\end{matrix}
$$

has a **unique solution** $\impliedby \text{rank}(A) = n \quad \land \quad \text{rank}(C) = p$

Algorithm based on block-LU decomposition
$$
\begin{aligned}
\begin{bmatrix}
A^\top A & C^\top \\
C & 0
\end{bmatrix}
&=
\begin{bmatrix}
I & 0 \\
Y & I \\
\end{bmatrix}
\begin{bmatrix}
U_1 & X \\
0 & U_2 \\
\end{bmatrix}
\\ \,\\ \,\\
U_1 &= A^\top A \\
X &= C^\top \\
Y U_1 = C &\iff U_1^\top Y = C^\top \quad [p m \times n \ \text{-LSE}] \\
YX + U_2 = 0 
\end{aligned}
$$
 **Remark:**

> if $A$ large, sparse $\implies$ don’t compute $A^\top A$
> but solve Extended Normal Equations with auxiliary unknown $r \ \colon = Ax-b$ (residual)
> $$
> \begin{bmatrix}
> -I & A & 0 \\
> A^\top & 0 & C^\top \\
> 0 & C & 0
> \end{bmatrix}
> \begin{bmatrix}
> r \\
> x \\
> m \\
> \end{bmatrix}
> =
> \begin{bmatrix}
> b \\
> 0 \\
> d \\
> \end{bmatrix}
> \quad \hat{=} \quad \text{Extended Augmented Normal Equations}
> $$

### 3.6.2 Solution via SVD

**Constraint:**

$Cx = d\quad $ with $C \in \R^{p,n}, \ p \lt n, \ \text{rank}(C) = p$
$$
x \in x_0 + \underbrace{\mathcal{N}(C)}_{\small{\impliedby \ \text{SVD}}}
$$
where $x_0$ is a particular solution of $Cx_0 = d$

Full SVD:
$$
\begin{aligned}
C &= U \begin{bmatrix} \Sigma_p & 0 \end{bmatrix} 
\underbrace{\begin{bmatrix} V_1^\top \\ V_2^\top \end{bmatrix}}_{=V}
\qquad \begin{aligned} 
\text{orth.} \ U &\in \R^{p,p} \\ 
\text{diag} \ \Sigma &\in \R^{p,p} \\
\text{orth.} \ V &\in \R^{n,n} \\ 
\end{aligned} \\

\mathcal{N}(C) &= \mathcal{R}(V_2) \\ \,\\
x &= x_0 + V_2y, \quad y \in \R^{n-p} \\ \,\\

\Vert Ax -b \Vert_2 &= \Vert A (x_0 + V_2y) - b \Vert_2 = \Vert \underbrace{AVy}_{m \times (n-p)} - (b - Ax_0) \Vert_2 \to \min \\
&\hat{=} \ \text{std. linear LSQ in} \ y \in \R^{n-p}
\end{aligned}
$$
**Remark:**

Finding $x_0 \in \R^n$
$$
\begin{aligned}
x_0 \in \mathcal{N}(C)^\perp &= \mathcal{R}(V_1) \\
x_0 &= V_1 z, \qquad z \in \R^p \\ \,\\

Cx = d \implies CV_1z &= d \\
\iff U \begin{bmatrix} \Sigma_p & 0 \end{bmatrix} \underbrace{\begin{bmatrix} V_1^\top \\ V_2^\top\end{bmatrix}V_1}_{= \begin{bmatrix} I_p \\ 0\end{bmatrix}} z &= d \\
\iff U \Sigma_p z &= d \\
\overset{\Sigma_p \in \R^{p,p} \ \text{regular} \\ \impliedby \text{rank}(C) = p}{\iff} z &= \Sigma_p^{-1} U^\top d \\ \,\\

x_0 &= V_1 \Sigma_p^{-1} U^\top d
\end{aligned}
$$


