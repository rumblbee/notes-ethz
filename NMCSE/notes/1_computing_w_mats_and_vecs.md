# 1 Computing with Matrices and Vectors

## 1.1 Fundamentals

### 1.1.1 Notations

#### Vectors
$$
x \in \mathbb{K}^n \quad (\mathbb{K} = \R, \C) \\
\\
(x)_i \, \hat{=}\,  i\text{-th component of } x, \quad i= 1, \dots, n
$$

#### Matrices
$$
A \in \mathbb{K}^{n,m} \\
\,\\
\text{Mat.Element: } (A)_{i,j} \colon= a_{i,j}, \quad 1 \leq i \leq n, 1 \leq j \leq m \\
\,\\
\text{Mat.Row: } (A)_{i,} \colon= [a_{i,1}, \dots, a_{i,m}] \\
\,\\
\text{Mat.Col: } (A)_{,j} \colon= [a_{1,j}, \dots, a_{n,j}]^\top \\
\,\\
\text{Mat.Block: } (A)_{k:l,r:s} \colon= [a_{ij}]_{\begin{smallmatrix} i &= k, \dots, l \\ j &= r, \dots, s \end{smallmatrix}} \\
\quad \implies (A)_{k:l,r:s} \in \mathbb{K}^{l-k+1,r-s+1}
$$

#### Kronecker Symbol
$$
\delta_{ij} \colon= 1 \iff i = j \\
\,\\
\delta_{ij} \colon= 0 \iff i \neq j \\
$$

### 1.1.2 Classes of Matrices

#### Identity matrix
$$
\textbf{I} \colon= \textbf{I}_n \in \mathbb{K}^{n,n}
$$

#### Zero Matrix
$$
\textbf{O} \colon= \textbf{O}_{n,m} \in \mathbb{K}^{n,m}
$$

#### Diagonal Matrix
$$
\text{diag}(d_1, \dots, d_n) \in \mathbb{K}^{n,n}, \quad d_j \in \mathbb{K}, \quad j=1, \dots, n
$$

![matrix classes](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/matrix_classes.png)

#### Skew Symmetric Matrix
$$
A \in \mathbb{K}^{n,n} \\ \,\\
A^\top = -A
$$

#### Symmetric / Hermitian Matrix
$$
A \in \mathbb{K}^{n,n} \\ \,\\
A^H = A \\ \,\\
\mathbb{K} = \R \implies \text{symmetric} \\ \,\\
\mathbb{K} = \C \implies \text{hermitian}
$$

#### Symmetric Positive (Semi-) Definite Matrix
$$
A \in \mathbb{K}^{n,n}, n \in \N \\
\,\\
\begin{aligned}
A \leftarrow \text{s.p.d.} 
&\impliedby A = A^H \quad \land \quad \forall x \in \mathbb{K}^n \colon x^HAx \gt 0 \iff x \neq 0 \\ 
\,\\
A \leftarrow \text{s.p.s.d.}
&\impliedby A = A^H \quad \land \quad \forall x \in \mathbb{K}^n \colon x^HAx \geq 0
\end{aligned}
$$

##### L. 1.1.2.7 - Necessary Conditions for s.p.d.
$$
\text{symmetric / hermitian positive definite } M = M^H \in \mathbb{K}^{n,n} \\ \,\\
\begin{aligned}
&\implies m_{ii} \ge 0 \quad \forall 1 \leq i \leq n \\\,\\
&\implies m_{ii}m_{jj}-\lvert m_{ij} \rvert^2 \gt 0 \quad \forall 1 \leq i \lt j \leq n \\\,\\
&\implies \forall \text{eigenvalues of } M \leftarrow \text{positive}
\end{aligned}
$$

## 1.2 Software and Libraries

### 1.2.1 Eigen

[Eigen Documentation](http://eigen.tuxfamily.org/dox/)

### 1.2.3 (Dense) Matrix Storage Formats

#### Row Major (C-arrays, python)
$$
A = \begin{bmatrix} 1&2&3 \\ 4&5&6 \\ 7&8&9 \end{bmatrix} \implies \text{Mem.: } 1|2|3|4|5|6|7|8|9
$$

##### Index Mapping (C++)
$$
(i,j) \mapsto i*\text{cols} + j
$$
> lin.Alg. $\implies$ subtract one!

#### Column Major (Eigen, Fortran)
$$
A = \begin{bmatrix} 1&2&3 \\ 4&5&6 \\ 7&8&9 \end{bmatrix} \implies \text{Mem.: } 1|4|7|2|5|8|3|6|9
$$

##### Index Mapping (C++)
$$
(i,j) \mapsto i*\text{rows} + j
$$
> lin.Alg. $\implies$ subtract one!

## 1.4 Computational Effort

$\#$ elementary operations ($+$,$-$,$*$,$/$, ...) $\not\approx$ runtime

runtime $\impliedby$ $\begin{cases} \text{memory access pattern} \\ \text{vectorization / pipelining} \end{cases}$

### 1.4.1 Asymptotic Complexity
> The **asymptotic complexity** of an algorithm characterises the worst-case dependence of its computational effor on one or more **problem size parameters** when these tend to $\infty$

> Asymptotic complexity predicts the dependence of runtime on problem size for 
> - large problems
> - a concrete implementation

#### Notation:

$$
\text{cost}(n) = O(n^\alpha), \quad \alpha \gt 0, \quad n \to \infty \\\,\\
\iff \exists C \gt 0, n_0 \in \N \colon quad \text{cost}(n) \leq Cn^\alpha \quad \forall n \gt n_0
$$

#### Tacit Sharpness Assumption
$$
\text{cost}(n) \neq = O(n^\beta), \quad \forall \beta \lt \alpha
$$

### 1.4.2 Computational Cost of Basic Numerical LA Operations
![](/home/bluejay/NMCSE/notes/imgs/cost_basic_num_LA_ops.png)

### 1.4.3 Tricks to improve Complexity

#### Ex. 1.4.3.1 - Exploit Associativity
$$
a, b, x \in \R^n \quad \text{column vectors} \\\,\\
y = (ab^\top)x \quad = \quad a(b^\top x) \\\,\\
\begin{aligned}
(ab^\top)x &\implies O(n^2) \quad \text{unnecessary tensor product!} \\\,\\
a(b^\top x) &\implies O(n)
\end{aligned}
$$

#### Ex. 1.4.3.5 Hidden Summation
$$
A,B \in \R^{n,p}, \quad p \ll n \\
x \in \R^n \\\,\\
y = \text{triu}(AB^\top)x \quad \implies O(n^2p)
$$

Videos Week 1 (14.09.-18.09.): 1.4 Computational Effort
==TODO: WATCH AGAIN==

#### Ex. 1.4.3.8 Reuse of Intermediate Results

##### Kronecker Product $A \otimes B$
$$
A \in \mathbb{K}^{m,n}, \quad B \in \mathbb{K}^{l,k}, \quad m,n,l,k \in \N \\\,\\

A \otimes B \colon= \begin{bmatrix}
(A)_{1,1}B & (A)_{1,2}B & \cdots & \cdots & (A)_{1,n}B \\
(A)_{2,1}B & (A)_{2,2}B & \cdots & \cdots & (A)_{2,n}B \\
\vdots & \vdots & & & \vdots \\
(A)_{m,1}B & (A)_{m,2}B & \cdots & \cdots & (A)_{m,n}B \\
\end{bmatrix} \in \mathbb{K}^{ml, nk}
$$

##### Task
> $A,B \in \R^{n,n}, x \in \R^{n^2}$
>
> compute
> $$
> y = (A \otimes B)x = \begin{bmatrix}
> (A)_{1,1}Bx^1 & (A)_{1,2}Bx^2 & \cdots & \cdots & (A)_{1,n}Bx^n \\
> (A)_{2,1}Bx^1 & (A)_{2,2}Bx^1 & \cdots & \cdots & (A)_{2,n}Bx^n \\
> \vdots & \vdots & & & \vdots \\
> (A)_{m,1}Bx^1 & (A)_{m,2}Bx^2 & \cdots & \cdots & (A)_{m,n}Bx^n \\
> \end{bmatrix}
> $$
> 
> where $x$ is a partitioned vector s.t.:
> $$
> x \colon= \begin{bmatrix} x^1 \\ x^2 \\ \vdots \\ x^n \end{bmatrix} \in \R^{n^2} \quad x^l \in \R^n
> $$

##### Idea
> precompute $Bx^l \quad l = 1 \dots n$
>
> $\implies Z = B \, \underbrace{\begin{bmatrix} x^1 & \dots & x^n \end{bmatrix}}_{\in \R^{n,n}} = \begin{bmatrix} Bx^1 & \dots & Bx^n \end{bmatrix}$
>
> $\implies O(n^3)$

$$
(A)_{1,1}Bx^1 + (A)_{1,2}Bx^2 + \dots + (A)_{1,n}Bx^n = Z (A)_{1,}^\top \\\,\\

\implies Y = Z A^\top \in \R^{n,n} \implies O(n^3) \\\,\\

y = \begin{bmatrix}(Y)_{,1} \\ \vdots \\ (Y)_{,n} \end{bmatrix} \in \R^{n^2}
$$

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/efficient_mult_of_kronecker_prod.png)

## 1.5 Machine Arithmetic and Consequences

### 1.5.1 Experiment: Loss of Orthogonality

@NCSEVideo_1_5_MachineArithmetic.pdf

### 1.5.2 Machine Numbers $\mathbb{M}$

computers $\leftarrow$ finite automata
$\implies$ cannot compute in $\R$
$\implies$ Numerical LA $\neq$ LA

computers compute in $\mathbb{M} \leftarrow \begin{cases} \text{finite} &\implies \text{overflow / underflow} \\ \text{discrete in } \R &\implies \text{round-off errors} \end{cases}$

### 1.5.3 Round-off Errors

op $\hat{=}$ elementary arithmetic operation $\in \{+,-,*,/\}$
but
$$
\text{op} \colon \mathbb{M} \times \mathbb{M} \not\to \mathbb{M} \\\,\\

\implies \mathbb{M} \text{ not closed under elementary arithmetic operations}
$$

thus Hardware replaces $\text{op}$ with approximation $\widetilde{\text{op}}$ which is the rounded operation $\text{rd} \circ \text{op}$
$$
\widetilde{\text{op}} \colon= \text{rd} \circ \text{op} \\\,\\

\text{rd} \colon= \begin{cases} \R &\to \mathbb{M} \\ x &\mapsto \max \text{arg}\min_{\tilde{x} \in \mathbb{M}} \lvert x - \tilde{x} \rvert \end{cases}
$$

relative error of $\widetilde{\text{op}}$ can be controlled

#### Relative Error

==TODO PAGE 3 https://www.sam.math.ethz.ch/~grsam/NCSE20/NCSEVideo_1_5_MachineArithmetic.pdf==

##### Minimize Relative Error

for example in a difference quotient (assuming $f \colon \R \to \R$ is smooth)
$$
\frac{f(x+h) - f(x)}{h} \\\,\\
h \approx \sqrt{\text{EPS}} \implies \min \text{rel.error}
$$

###### Note

> if we divide some number with a relative error by a really small number, 
> then we get a big number with a huge relative error!

##### \# correct decimal digits
$$
\# \text{ correct decimal digits} = \log_{10} \text{rel.err}
$$

#### Guaranteed Machine Precision

Machine Precision EPS $$\colon= \max_{x \in \lvert \mathbb{M} \rvert} \frac{\lvert \text{rd}(x) -x \lvert}{\lvert x \rvert} \approx 2.2 \cdot 10^{-16}$$ for double

where $\lvert \mathbb{M} \rvert \colon= [\min\{\lvert x \rvert,  x \in \mathbb{M}\}, \max\{\lvert x \rvert, x \in \mathbb{M}\}]$
$\iff \text{rd}(x) = x ( 1 + \epsilon) \quad$ with $\lvert \epsilon \rvert  \le$ EPS

#### Axiom of Roundoff Errors

Machine Precision EPS $\implies \forall$ elementary arithmetic operations $\star \in \{+,-,\cdot,/\}$ and $\forall$ hard-wired functions $f \in \{\exp, \sin, \cos, \log, \dots\}$
$$
x \widetilde{\star} y = (x \star y)(1 + \delta) \quad \land \quad 
\widetilde{f}(x) = f(x) (1 + \delta) \quad \forall x,y \in \mathbb{M} \quad \text{with} \lvert \delta \rvert \lt \text{EPS}
$$

#### Remark

Impact of roundoff can be vastly different for different implementations


#### Remark 1.5.3.15

Testing for ```(x == 0)``` must be replaced with testing for relative smallness

### 1.5.4 Cancellation

> Extreme **amplification** of relative errors during the **subtraction** of numbers of **equal size**

#### 1.5.4.16 Avoiding Cancellation

> with knowledge of analysis we can most of the time rewrite the equation to an equivalent form,
> which does not depend on a subtraction of equal sized numbers

##### Ex. 1.5.4.19 
$$
\int_0^x \sin t dt = \underbrace{1 - \cos x}_{\text{cancellation}} = \underbrace{2 \sin^2(\frac{x}{2})}_{\text{no cancellation}}
$$

##### Ex. 1.5.4.26 - Trading Cancellation for Approximation

$$
I(a) = \int_0^a e^{at} dt = \frac{e^a - 1}{a} \quad \implies \text{cancellation for } a \approx 0
$$

Idea: **Approximation** by Taylor polynomial for $a \approx 0$
$f \colon \R \to \R$ smooth
$$
f(x_0 + h) = \sum_{k=0}^m \frac{f^{(k)}(x_0)}{k!}h^k + \underbrace{\frac{1}{(m+1)!}f^{(m+1)}(\xi)h^{m+1}}_{\text{remainder term, } \xi \in ]x_0, x_0 + h[}
$$

applay with $f(x) = e^x \implies f^{(k)}(x) = e^x, \quad x_0 = 0, \quad h = a$

$$
I(a) = \frac{e^a - 1}{a} = \underbrace{\sum_{k=0}^m \frac{1}{(k+1)!}a^k}_{\widetilde{I}_m(a) \approx I(a)} + R_m(a) \\
\,\\
R_m(a) = \frac{1}{(m+1)!}e^\xi a^m \quad \text{for some } 0 \leq \xi \leq a
$$
But how to choose $m$ ?

Goal: relative **truncation error** $\leq$ tolerance $\text{tol}$

$$
\text{rel.err} = \frac{\lvert I(a) - \widetilde{I}_m(a) \rvert}{\lvert I(a) \rvert} = \frac{R_m(a)}{\frac{e^a -1 }{a}} \leq \frac{1}{(m+1)!}e^\xi a^m\leq \frac{1}{(m+1)!}e^a a^m \leq \text{tol}
$$
Lets suppose $a= 10^{-3}$

| $m$ |  1 | 2 | 3 | 4 |
| --- | --- | --- | --- | --- |
| rel.error | 1.0010e-03 | 5.0050e-07 | 1.6683e-10 | 4.1708e-14 |

thus if we want 10 correct decimal digits, we chose $m = 3$