# Direct Methods for Square Linear Systems of Equations


## 2.1 Introduction - Linear System of Equations (LSE)

square system / coefficient matrix $A \in \mathbb{K}^{n,n}$
$\rightarrow$ $\#$ equations = $\#$ unknown = $n$
$\rightarrow$ scalar equation per row $(A)_i^T x = (b)_i \quad i = 1 \dots n$

vector of unknowns $x \in \mathbb{K}^n$

RHS vector $b \in \mathbb{K}^n$


$$
Ax = b \\
$$

### 2.1.0.3 Ex: Nodal Analysis of Linear Electric Circuits - Frequency Domain

[Tablet Notes](https://www.sam.math.ethz.ch/~grsam/NCSE20/NCSEVideo_2_1_0_3_Example_Nodal_Analysis.pdf)


## 2.2 Square LSE - Theory

### 2.2.1 Existence and Uniqueness of Solutions

#### Def. Invertible Matrix

> $$
A \in \mathbb{K}^{n,n} \quad \text{invertible / regular } \quad \colon\iff \quad \exists ! B \in \mathbb{K}^{n,n} \colon AB = BA = I \\
> $$
> $B \colon= A^{-1} \leftarrow$ inverse of $A$

#### Regular Matrix $A \in \mathbb{K}^{n,n}$

> $$
\begin{aligned}
&\iff \text{det} A \neq 0 \\
&\iff \text{cols} \leftarrow \text{lin. independent} \\
&\iff \text{rows} \leftarrow \text{lin. independent} \\
&\iff \text{Nullspace } \mathcal{N}(A) = \{x \in \mathbb{K}^n \colon Az = 0 \} = \{0\} \\
\end{aligned}
> $$


## 2.3 Gaussian Elimination (GE)

### 2.3.1 Basic Algorithm

> successive row transformation of LSE $Ax = b$ to convert it into triangle form
> $Ax = b \implies [ A | b ] -\text{forward elimination}\rightarrow [ \text{triu}(A) | \tilde{b} ] -\text{back substitution}\rightarrow [ \textbf{1}| x ]$
>
> fwd elimination $\in O(n^3)$
> back elimination $\in O(n^2)$

#### 2.3.1.14 Remark: Block GE

$$
Ax = b \quad A \in \R^{n,n} \\\,\\
A = \begin{bmatrix} A_{11} A_{12} \\ A_{21} A_{22} \end{bmatrix} \quad A_{11} \in \R^{k,k} \\\,\\
\text{assume: } A_{11} \leftarrow \text{regular}
$$

Recall:

$$
\begin{bmatrix} A_{11} A_{12} \\ A_{21} A_{22} \end{bmatrix} \begin{bmatrix} B_{11} B_{12} \\ B_{21} B_{22} \end{bmatrix} = \begin{bmatrix} A_{11}B_{11} + A_{12}B_{21} \quad A_{11}B_{12}+ A_{12}B_{22} \\ A_{21}B_{11}+ A_{22}B_{21} \quad A_{21}B_{12}+ A_{22}B_{22} \end{bmatrix}
$$

Now: GE like for 2x2 LSE (except for commutativity, since mat.mult)
$$
\begin{aligned}
\left[\begin{array}{cc|c}  
A_{11} & A_{12} & b_1 \\
A_{21} & A_{22} & b_2 \\
\end{array}\right]

&\underrightarrow{\text{fwd.elim}}

\left[
\begin{array}{cc|c}
A_{11} & A_{12} & b_1 \\
0 & \color{green} A_{22} - A_{21}A_{11}^{-1}A_{12} & b_2 - A_{21}A_{11}^{-1}b_1
\end{array}
\right]

\\

&\underrightarrow{\text{bck.elim}}

\left[
\begin{array}{cc|c}
\mathbb{I} & 0 & A_{11}^{-1}(b_1 - A_{12} S^{-1} b_S) \\
0 & \mathbb{I} & S^{-1}b_S \\
\end{array}
\right]

\\\,\\

&\implies

\left[
\begin{array}{c}
 A_{11}^{-1}(b_1 - A_{12} S^{-1} b_S) \\
S^{-1}b_S \\
\end{array}
\right]

=

\left[
\begin{array}{c}
x_1 \\
x_2 \\
\end{array}
\right]

\in \R^n

\end{aligned}
$$

$\color{green}\text{Schur Complement}$ $S \colon= A_{22}-A_{21}A_{11}^{-1}A_{12}$

#### Remark 2.3.2.19 - Block LU Decomp

$$
\underbrace{
\begin{bmatrix}
A_{11} & A_{12} \\
A_{21} & A_{22} \\
\end{bmatrix}
=
\begin{bmatrix}
\mathbb{I} & 0 \\
A_{21}A_{11}^{-1} & \mathbb{I} \\
\end{bmatrix}
\begin{bmatrix}
A_{11} & A_{12} \\
0 & S \\
\end{bmatrix} }_{\text{block LU-Decomp.}}
$$

### 2.3.2 LU Decomposition

$\text{cost}(\text{LU}) = O(n^3)$

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/lu_decomp.png)

#### 2.3.2.15 Solving LSE with LU

$$
Ax = b \iff L(Ux) = b  \implies \begin{cases} 
Lz = b \\
Ux = z \\
\end{cases}
$$

1. LU-decomposition $A = LU$ $\implies \#$ elem.ops $= \frac{1}{3}n(n-1)(n+1) \implies O(n^3)$
2. fwd. substitution, solve $Lz = x$ $\implies$ $\#$ elem.ops $= \frac{1}{2} n(n-1) \implies O(n^2)$
3. back. substitution, solve $Ux = z$ $\implies \#$ elem.ops $= \frac{1}{2} n(n-1) \implies O(n^2)$

#### 2.5.0.4 GE / LU-dec. in Eigen

$A \in \R^{n,n}\leftrightarrow$ (square) matrix object

LSE with multiple r.h.s.: $AX = B \in \R^{n,l}$

lin.Alg | Eigen
:---: | :---:
$X = A^{-1}B$ | ```X = A.lu().solve(B)```

## 2.6 Exploiting Structure when Solving Linear Systems



#### 2.6.0.5 LSE with arrow matrix

$$
Ax = 
\begin{bmatrix}
D & c \\
b^\top & \alpha \\
\end{bmatrix}

\begin{bmatrix}
x_1 \\
\xi \\
\end{bmatrix}

= y \ \colon = 

\begin{bmatrix}
y_1\\
\eta \\
\end{bmatrix}

\,\\\,\\

\implies \xi = \frac{\eta- b^\top D^{-1}y_1}{\alpha- b^\top D^{-1}c} \\
\,\\
\implies x_1 = D^{-1}(y_1 - \xi c) \\
\,\\
\implies \text{cost} \in O(n)
$$

#### 2.6.0.12 LSE subject to low-rank modification

**Assume:** $Ax = b$ is easy to solve because

1. $A$ is of a special form
2. LU-decomp. is available

**Sought:** $\widetilde{x} \colon \quad \widetilde{A}\widetilde{x}= bd$ 

where $\widetilde{A}$ arises from $A$ by **changing a single entry** $(A)_{i^*, j^*}$(aka **rank 1 modification**)
$$
\widetilde{A} = A + z \cdot e_i \cdot e_j^\top
$$
where $e_i \cdot e_j^\top \leftarrow n \times n$ tensor product matrix of rank 1
basically a $n \times n$ matrix where everything is zero except the  entry $(A)_{i^*, j^*} = 1$
$$
\begin{bmatrix}
A & u \\
v^\top & -1 \\
\end{bmatrix}
\begin{bmatrix}
\widetilde{x} \\
\xi
\end{bmatrix}
=
\begin{bmatrix}
b \\
0
\end{bmatrix}
\,\\
\,\\
\implies \widetilde{x} = A^{-1}b - \frac{A^{-1}u(v^H(A^{-1}b))}{1 + v^H(A^{-1}u)}
$$

#### Lemma 2.6.0.21 - Sherman-Morrison-Woodbury formula

regular $A \in \mathbb{K}^{n,n}$
$U,V \in \mathbb{K}^{n,k},\quad n,k \in \N,\quad k \leq n$
$I + V^H A^{-1} U \leftarrow$regular
$$
(A+UV^H)^{-1} = A^{-1}-A^{-1}U(I + V^H A^{-1} U)^{-1}V^HA^{-1} \\
$$

## 2.7 Sparse Linear Systems

### 2.7.1 Spare Matrix Storage Formats

#### 2.7.1.1 COO / triplet format

```c++
struct Triplet {
    size_t i; // row index
    size_t j; // col index
    scalar_t a; // entry
};
using TripletMatrix = std::vector<Triplet>;
```

##### Note:
> repeated triplets allowed, we just sum them up

##### Matrix Vector Product - Triplet Format

```C++
void multTriplMatVec(const TripletMatrix &A, 
                     const vector<scalar_t> &x,
                     vector<scalar_t> &y) {
    for (size_t k = 0; k < A.size(); k++) {
        y[A[k].i] += A[k].a * x[A[k].j];
    }
}
```

#### 2.7.1.4 Compressed Row Storage (CRS)

$A \in \mathbb{K}^{n,n}$

```C++
struct CRS {
	std::vector<scalar_t> val; // size = nnz(A) = # non-zero entries of A
	std::vector<size_t> col_idx; // size = nnz(A)
	std::vector<size_t> row_ptr; // size = n + 1 && row_ptr[n+1] = nnz(A) + 1 (sentinel value)
}
```

![](/home/bluejay/ETH/subjects/semester_3/NMCSE/notes/imgs/crs.png)

### 2.7.2 Sparse Matrices in Eigen

```C++
#include <Eigen/Sparse>
Eigen::SparseMatrix<int, Eigen::ColMajor> Asp(rows, cols); // CCS (Compressed Column Storage)
Eigen::SparseMatrix<int, Eigen::RowMajor> Bsp(rows, cols); // CRS (Compressed Row Storage)
```

Setting Random Entry $\implies$ massive data movements

**Idea 1: Use Intermediate COO / Triplet Format**

```C++
std::vector<Eigen::Triplet <double>> triplets;
// ... fill the std::vector triplets ...
Eigen::SparseMatrix<double, Eigen::RowMajor> spMat(rows, cols);
spMat.setFromTriplets(triplets.begin(), triplets.end());
```

$\implies O(\# \text{triplets})$ complexity

**Idea 2: ```reserve()``` and ```insert()```, if nnz / per row (col) known beforehand**

```C++
unsigned int rows, cols, max_no_nnz_per_row;
// ...
SparseMatrix<double, RowMajor> mat(rows, cols);
mat.reserve(RowVectorXi::Constant(cols, max_no_nnz_per_row)); // Allocation of enough space
//  do many (incremental) initializations
for ( ... ) {
	mat.insert(i,j) = value_ij;
	mat.coeffRef(i,j) += increment_ij;
} // for loop in O(1) <= enough space reserv()'d
mat.makeCompressed(); // squeeze out zeros
```

### 2.7.3 Direct Solution of Square Linear Systems of Equations

**Assume:** system matrix in sparse format
$\implies$ location of zero entries $\implies$ exploitable by sparse elimination techniques

```C++
using SparseMatrix = Eigen::SparseMatrix<double>;
// Perform Sparse Elimination
void sparse_solve(const SparseMatrix &A, const VectorXd &b, VectorXd&x) {
	Eigen::SparseLU<SparseMatrix> solver(A); // expensive part
	if (solver.info() != Eigen::Success)
		throw "Matrix factorization failed!";
	
	x = solver.solve(b); // triangular solves (relatively cheap)
}
```