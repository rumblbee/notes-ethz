# Numerical Methods

## Def. Numerical Methods
> Algorithms for evaluating functions and solving equations in $\R$
>
> Analysis | Linear Algebra | Programming

## C++ Readings
- C++ primer (5th Ed.)
- The C++ Standard Library (2nd Ed.)

