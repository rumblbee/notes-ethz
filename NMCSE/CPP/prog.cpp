#include <Eigen/Dense>
#include <iostream>
 
using namespace Eigen;
using namespace std;
 
int main()
{
  /*
  Matrix3i m = Matrix3i::Random(3,3);
  cout << m << endl;
  cout << endl;
  cout << m.diagonal() << endl;
  cout << endl;
  m.diagonal().setConstant(3);
  cout << m.diagonal() << endl;
  */

  MatrixXd A = MatrixXd::Random(3,5);
  MatrixXd B = MatrixXd::Random(5,3);

  Array2i rwA(2,1);
  Array2i rwB(2,1);

  rwA << A.rows(), A.cols();
  rwB << B.rows(), B.cols();

  int minA = rwA.minCoeff();
  int minB = rwB.minCoeff();

  A.diagonal().setConstant(0);
  B.diagonal().setConstant(0);

  /*
  cout << "A : " << endl << A << endl << endl;

  cout << "A.col(1) : " << endl << A.col(1) << endl << endl;

  cout << "A.col(1).sum() = " << A.col(1).sum() << endl << endl;

  cout << "A.diagonal().segment(1,1) : " << endl << A.diagonal().segment(1,1) << endl;

  A.diagonal().segment(1,1) = 2.0;

  cout << "A : " << endl << A << endl << endl;

  */

  for (int i = 0; i < minA; ++i) {
    A(i,i) = -1 * A.col(i).sum();
  }

  for (int i = 0; i < minB; ++i) {
    B(i,i) = -1 * B.col(i).sum();
  }

  cout << A << endl << endl;

  for (int i = 0; i < minA; ++i) {
    cout << A.col(i).sum() << "  ";
  }

  cout << endl << endl;

  cout << B << endl << endl;

  for (int i = 0; i < minB; ++i) {
    cout << B.col(i).sum() << "  ";
  }

  cout << endl << endl;

}

