#include <Eigen/Dense>
#include <iostream>
using namespace Eigen;
using namespace std;

int main() {

    int rows = 5;
    int cols = 3;

    MatrixXd M = MatrixXd::Random(rows, cols);

    MatrixXd Mext(rows+1,cols+1);

    cout << "DEBUG:" << endl;
    cout << endl << MatrixXd::Zero(rows,1) << endl;
    cout << endl << MatrixXd::Zero(1, cols) << endl;
    cout << endl << MatrixXd::Ones(1,1) << endl;

    cout << "--------------------------------------------" << endl;
    
    Mext << M, 
            MatrixXd::Zero(rows,1),
            MatrixXd::Zero(1, cols),
            MatrixXd::Ones(1,1);
    
    cout << Mext << endl;

    cout << "--------------------------------------------" << endl;
    
    
}
