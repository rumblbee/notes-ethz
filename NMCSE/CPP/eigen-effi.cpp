#include <Eigen/Dense>
#include <Eigen/SVD>

#include <iostream>

using namespace Eigen;
using namespace std;

int main() {
    MatrixXd A(3,3);
    // A << 3,5,9,7,8,3,0,7,0,4,2,0;
    A << 1,2,3,4,5,6,7,8,9; 

    JacobiSVD<MatrixXd> svd(A, ComputeFullU | ComputeFullV);

    MatrixXd U = svd.matrixU();
    MatrixXd V = svd.matrixV();
    VectorXd sv = svd.singularValues();
    MatrixXd S = sv.asDiagonal();

    cout << "A = USV^H" << endl << endl;
    cout << A << endl << endl << " = " << endl << endl << U << endl << endl << " " << endl << endl << S << endl << endl << " " << endl << endl << V.transpose() << endl << endl;

    /*
    VectorXd out = V.transpose() * V.col(1);
    
    cout << out.squaredNorm() << endl;
    */

    cout << (sv(0)*sv(0)) * (V.col(1).squaredNorm()) << endl;
    cout << sv(0);
    return 0;
}
