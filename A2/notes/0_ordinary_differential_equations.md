# Analysis 2 

# Ordinary Differential Equations

## Differential Equation

> Equation for a function $f$ that relates the values of $f$ of $x$, $f(x)$ to the values of its derivatives, at the same point $f’(x), f’‘(x), ...$

### Ex:
> 1) 
> $$
> f’(x) = f(x)
> $$
> 
> 2) 
> $$
> f''(x) = f(x)
> $$
> 
> 3) consider mass falling under influence of gravity.
> Netwton's law $\implies $ 

### Notation:
> $y, y', y'', ..., y^(k)$ instead of $f(x), f'(x), ..., f^{(k)}(x)$

> ### Def.: Order of a Diff.Eq
> $\impliedby$ largest derivative present in the queation

> ### Ex:
> 
> #### Partial Differential Equation (PDE)
> $$
> a^2 \frac{\delta^2 u}{\delta x^2} = \frac{\delta u}{\delta t}
> $$
> 
> #### Not an Ordinary Differential Equation (ODE)
> $$
> f'(x+2) = f(x)
> $$
> 
> because it realtes the to different arguments of $f(x)$

> In general the solution to an ODE is not unique
> But if we are given "initial conditions" $\implies$ unique solutions
> 
> the order of an ODE $\implies \#$ initial conditions

## Linear Differential Equations

### Def.: Linear Ordinary Differential Equation (LODE) of Order $k$ on interval $I \sub \R$
> $$
> y^{(k)} + a_{k-1}(x)y^{(k-1)} + \cdots + a_1(x)y' + a_0(x) y = b(x) \\
> \iff \\
> D(f) = b(x)
> $$
>
> where 
> $$
> \left.
> \begin{array}{ll}
> a_j(x) \quad j = 0, \dots, k \\
> b(x)
> \end{array}
> \right \} \text{ are continous from } I \to \C
> $$
>
> D ==PAGE 7==
>
> $b = 0 \implies$ ==homogeneous== otherwise inhomogeneous

### Remark
> $a_i(x), b(x) \leftarrow$ real-values
> $\implies$ interessted in soltuion $f \colon I \to \R$

### Remark: Solving a LODE
> $\implies$ finding all funtions $f \colon I \to \C$ $k$ times differentiable s.t.: $\forall x \in I \colon$
> $$
> f^{(k)} + a_{k-1}(x)f^{(k-1)}(x) + \cdots + a_0(x) f(x) = b(x)
> $$

### Def.: Initial Condition for a LODE
> is a set of equations, which specify values of $y, y', \dots, y^{(k-1)}$ at some initial point $x_0$
> 
> $$
> y(x_0) = y_0 \\
> y'(x_0) = y_1 \\
> \vdots \\
> y^{(k-1)}(x_0) = y_{k-1}
> $$
> 
> given that $\implies$ ==Initial Value Problem==

### Main Result about LODEs

#### Theorem (2.2.3)
> open Interval $I \sub \R$
> integer $k \geq 1$
> LODE $y^{(k)} + a_{k-1}(x)y^{(k-1)} + \cdots + a_1(x)y' + a_0(x) y = b(x)$
> where $a_i(x), b(x)$ are continous functions
> 
> 1 )
> $S_0 \leftarrow$ set of solutions when $b = 0$
> $\implies S_0 \leftarrow$ ==Vector Space== of dimension $k$
> 
> 2 )
> For any initial condisons, i.e.
> for any choise of $x_0 \in I$ and $(y_0, \dots, y_{k-1}) \in \C^k$
> $\implies \exists !$ solution $f \in S_0 \colon$
> $\quad f(x_0) = y_0, \cdots f^{(k)} = y_k$
> 
> 3 )
> for an arbitrary $b$ 
> $\implies$ set of solutions of  theLODE $= S_b = \{f + f_p \vert f \in S-0\}$
> where $f_p \leftarrow$ one "particular" solution of the LODE 

### Remark: 
> $Ax = b$ similar to 3 )

### Remark
> to see 3 )
> ==PAGE 9/10==
> 

### Remark: Superposition Principle
> the linearity of the LODE $\implies$ "superposition" principle
>
> suppose we have 2 different functions $b_1(x), b_2(x)$ on the RHS with say solutions $f_1, f_2$
> $$
> D f_1 = b_1 \\
> D f_2 = b_2  \\
> \\
> \implies f_1 + f_2$ solves $Df = b_1 + b_2
> $$
> 

### Remark: Solution Checking (with a possible solution)
> plug it in and see for yourself

## Linear Differential Equations of Order 1

> consider:
> $$
> y' + ay = b
> $$
> where:
> $a, b \leftarrow$ continous functions

### Solution
> 1 )
> find solutions of the corresponding homogeneous equation
> $$
> y' + ay = 0
> $$
> 
> #### Note
> $f \leftarrow$ solution $\implies z f \quad \forall z \in \C$ is also a solution
> $f' + af = 0 \implies (zf)' + a(zf) = z(f'+af) = 0$
> 
> 2 )
> find a particular solution 
> $$
> f_p \colon I \to \C \\
> \quad f_p + a f_p = b 
> $$
> 

#### Homogeneous Solution:
> $$
> \begin{aligned}
> y' + ay &= 0 \\
> y' &= -ay \\
> \frac{y'(x)}{y(x)} &= -a(x) \\
> \int \frac{y'(x)}{y(x)} dx &= - \int a(x) dx \colon= A(x) \\
> \\
> \implies \ln \lvert y(x) \rvert &= - A(x) + C \\
> \implies y &= z \exp(-A(x)) \quad \text{for some constant } z \\
> \end{aligned}
> $$
>

##### Proposition
> any solution of 
> $$
> y' + a(x)y = 0
> $$
> is of the form
> $$
> f(x) = z \exp(-A(x))
> $$
> where 
> $A(x) \leftarrow$ a primitive of $a(x)$
> $z \in \C$

#### Inhomogenous Equation
> $$
> y' + ay = b
> $$
> 
##### Educates Guess
> $b(x) \leftarrow$ polynomial $\implies f_p \leftarrow$ polynomial
> $b(x) \leftarrow$ trigonometric $\implies f_p \leftarrow$ trigonometric
> 

##### Variation of Constants / Parameters
> a )
> assume
> $$
> f_p = z(x) \exp(-A(x))
> $$
> for some function $z \colon I \to \C$
> 
> b )
> put this into the equation
> $$
> y' + ay = b
> $$
> and see what if forces $z(x)$ to satisfy
> 
> $$
> f_p = z(x) e^{-A(x)} = y \\
> y' = z'(x) e^{-A(x)} + z(x)(-A(x))' e^{-A(x)} \\
> y' = z'(x) e^{-A(x)} - z(x)a(x)e^{-A(x)} \\
> y' = e^{-A(x)} [z'(x) - z(x)a(x)] \\
> ay = a(x) z(x) e^{-A(x)} \\
> $$
> 
> $$
> y'+ay = z'(x)e^{-A(x)} = b(x) \\
> z'(x) = b(x)e^{A(x)} \\
> $$
> where $z(x)$ is a primitive of $b(x)e^{A(x)}$

## Linear Differential Equations with constant coeficients

$$
y^{(k)}+a_{k-1}y^{(k-1)}+ \dots + a_0y = b(x) \\
\\
a_0, a_1, \dots, a_{k-1} \in \C \\
$$

we know that any solution of $y(x)$ has the form
$$
y(x) = y_h + y_p
$$

where $y_h(x)$ satisfies:
$$
y_h^{(k)} + \dots + a_0y_h = 0 
$$

### Idea
recall 
$$
y' + ay = 0
$$
has a solution which is
$$
y = e^{-ax}
$$

==p7==

### Def.: Companion of the characteristic polynomial of (*)
for a linear ODE with constant coefficient
$$

$$

### Theorem
Let $\lambda_1, \dots, \lambda_r$ be pointwise distinct RReigenvalues of the characteristic polymomial $P(\lambda)$ of
$$
y^{(k)}
$$

--- START LECTURE  3 ---

### Rk.: 
> if $a_i$‘s are real its natural to look at real solutions
>
> $\alpha = \beta + i \gamma \leftarrow$ complex root of characteristic Polynomial $P(\lambda)$
> $\implies \bar{\alpha} = \beta - i \gamma$ also
> 
> Hence $f_1 \colon= e^{\alpha x}, f_2 \colon=e^{\bar{\alpha} x} are 2 solutions
> $$
> \begin{aligned}
> e^{\alpha x} &= e^{\beta x} \cdot e^{i \gamma x} \\
> &= e^{\beta x} [\cos \gamma x + i \sin \gamma x] \\
> \\
> e^{\bar{\alpha} x} &= e^{\beta x} \cdot e^{-i \gamma x} \\
> &= e^{\beta x} [\cos \gamma x - i \sin \gamma x] \\
> \end{aligned}
> $$


==PAGE 1 & 2==

we can use as solutions either
$$
e^{ix}, xe^{ix}, e^{-ix}, xe^{-ix}
$$

or

$$
\cos x, \sin x, x \cos x, x \sin x
$$

$$
y_h  = z_1 \sin x + z_2 \cos x + z_3 x \sin x + z_4 x \cos x
$$

suppose we are given extra conditions on $y$
$$
y(0) = 1 \quad y'(0) = 3 \\
y(\pi) = 2 \quad y'(\pi) = 0
$$

$$
\begin{aligned}
y_h(0) &= z_1 \sin 0 + z_2 \cos 0 = 1 \\
\implies z_2 &= 1 \\
\\
y(\pi) &= z_1 \sin \pi + z_2 \cos \pi + z_3 \pi \sin \pi + z_4 \pi \cos \pi = 2 \\
&= -z_x - z_4 \pi = 2 \\
&= -1 - \pi z_4 = 2 \\
\pi z_4 &= -3 \\
\implies z_4 &= -3 / \pi 
\end{aligned}
$$

== PAGE 3==

# Differential Calculus in $\R^n$



# Integral Calculus in $\R^n$

